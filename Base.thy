theory Base
  imports Main
begin

type_synonym ('var,'val) Mem = "'var \<Rightarrow> 'val"
type_synonym ('var,'sec) Ctx = "'var \<Rightarrow> 'sec"
type_synonym ('var,'val,'sec) Type = "('var,'val) Mem \<Rightarrow> 'sec"

definition ter
  where "ter P a b \<equiv> if P then a else b"

declare ter_def [simp]

definition supl :: "'a::bounded_lattice list \<Rightarrow> 'a"
  where "supl l \<equiv> fold sup l bot"

end