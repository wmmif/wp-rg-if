theory Typed_Predicate_Language
  imports Base
begin

datatype ('var, 'val,'sec) pred =
  Pb "bool" 
  | PDisj "('var,'val,'sec) pred" "('var,'val,'sec) pred" (infixr "\<or>\<^sub>p" 30)
  | PConj "('var,'val,'sec) pred" "('var,'val,'sec) pred" (infixr "\<and>\<^sub>p" 35)
  | PImp  "('var,'val,'sec) pred" "('var,'val,'sec) pred" (infixr "\<longrightarrow>\<^sub>p" 25)
  | PNeg  "('var,'val,'sec) pred"
  | PAllVar  "'val \<Rightarrow> ('var,'val,'sec) pred"
  | PAllSec  "'sec \<Rightarrow> ('var,'val,'sec) pred"
  | PCmp  "('val \<Rightarrow> 'val \<Rightarrow> bool)" "('var,'val,'sec) exp" "('var,'val,'sec) exp"
  | PSec  "('sec \<Rightarrow> 'sec \<Rightarrow> bool)" "('var,'val,'sec) secexp" "('var,'val,'sec) secexp"
and ('var,'val,'sec) secexp =
  Type "'var"
  | Sec "'sec"
  | SOp "('sec \<Rightarrow> 'sec \<Rightarrow> 'sec)" " ('var,'val,'sec) secexp" " ('var,'val,'sec) secexp"
  | STer "('var,'val,'sec) pred" "('var,'val,'sec) secexp" " ('var,'val,'sec) secexp"
and ('var,'val,'sec) exp =
  Var "'var"
  | Const "'val"
  | Op "('val \<Rightarrow> 'val \<Rightarrow> 'val)" "('var,'val,'sec) exp" "('var,'val,'sec) exp"
  | Ternary "('var,'val,'sec) pred" "('var,'val,'sec) exp" "('var,'val,'sec) exp"

primrec
  eval :: "('var,'val) Mem \<Rightarrow> ('var,'sec) Ctx \<Rightarrow> ('var,'val,'sec) exp \<Rightarrow> 'val" and
  test :: "('var,'val) Mem \<Rightarrow> ('var,'sec) Ctx \<Rightarrow> ('var,'val,'sec) pred \<Rightarrow> bool" and
  eval\<^sub>s :: "('var,'val) Mem \<Rightarrow> ('var,'sec) Ctx \<Rightarrow> ('var,'val,'sec) secexp \<Rightarrow> 'sec"
where
  "eval m \<Gamma> (Var v) = m v" |
  "eval m \<Gamma> (Const c) = c" |
  "eval m \<Gamma> (Op bop e f) = bop (eval m \<Gamma> e) (eval m \<Gamma> f)" |
  "eval m \<Gamma> (Ternary v e\<^sub>1 e\<^sub>2) = (ter (test m \<Gamma> v) (eval m \<Gamma> e\<^sub>1) (eval m \<Gamma> e\<^sub>2))" |
  "test m \<Gamma> (Pb v) = v" |
  "test m \<Gamma> (PDisj p q) = (test m \<Gamma> p \<or> test m \<Gamma> q)" |
  "test m \<Gamma> (PConj p q) = (test m \<Gamma> p \<and> test m \<Gamma> q)" |
  "test m \<Gamma> (PImp p q) = (test m \<Gamma> p \<longrightarrow> test m \<Gamma> q)" |
  "test m \<Gamma> (PNeg p) = (\<not> test m \<Gamma> p)" |
  "test m \<Gamma> (PCmp c e f) = (c (eval m \<Gamma> e) (eval m \<Gamma> f))" |
  "test m \<Gamma> (PSec c e f) = (c (eval\<^sub>s m \<Gamma> e) (eval\<^sub>s m \<Gamma> f))" |
  "test m \<Gamma> (PAllVar f) = (\<forall>v. test m \<Gamma> (f v))" |
  "test m \<Gamma> (PAllSec f) = (\<forall>v. test m \<Gamma> (f v))" |
  "eval\<^sub>s m \<Gamma> (Type v) = (\<Gamma> v)" |
  "eval\<^sub>s m \<Gamma> (Sec s) = s" |
  "eval\<^sub>s m \<Gamma> (SOp op a b) = (op (eval\<^sub>s m \<Gamma> a) (eval\<^sub>s m \<Gamma> b))" |
  "eval\<^sub>s m \<Gamma> (STer v s\<^sub>1 s\<^sub>2) = (ter (test m \<Gamma> v) (eval\<^sub>s m \<Gamma> s\<^sub>1) (eval\<^sub>s m \<Gamma> s\<^sub>2))"

primrec
  vars\<^sub>e :: "('var,'val,'sec) exp \<Rightarrow> ('var) set" and
  vars\<^sub>p :: "('var,'val,'sec) pred \<Rightarrow> ('var) set" and
  vars\<^sub>s :: "('var,'val,'sec) secexp \<Rightarrow> ('var) set"
where
  "vars\<^sub>e (Var v) = { v }" |
  "vars\<^sub>e (Const c) = {}" |
  "vars\<^sub>e (Op bop e f) = (vars\<^sub>e e \<union> vars\<^sub>e f)" |
  "vars\<^sub>e (Ternary v e\<^sub>1 e\<^sub>2) = (vars\<^sub>p v \<union> vars\<^sub>e e\<^sub>1 \<union> vars\<^sub>e e\<^sub>2)" |
  "vars\<^sub>p (Pb v) = {}" |
  "vars\<^sub>p (PNeg p) = vars\<^sub>p p" |
  "vars\<^sub>p (PConj p q) = (vars\<^sub>p p \<union> vars\<^sub>p q)" |
  "vars\<^sub>p (PDisj p q) = (vars\<^sub>p p \<union> vars\<^sub>p q)" |
  "vars\<^sub>p (PImp p q) = (vars\<^sub>p p \<union> vars\<^sub>p q)" |
  "vars\<^sub>p (PCmp cmp e f) = (vars\<^sub>e e \<union> vars\<^sub>e f)" |
  "vars\<^sub>p (PSec cmp e f) = (vars\<^sub>s e \<union> vars\<^sub>s f)" |
  "vars\<^sub>p (PAllVar f) = (\<Union>v. vars\<^sub>p (f v))" |
  "vars\<^sub>p (PAllSec f) = (\<Union>v. vars\<^sub>p (f v))" |
  "vars\<^sub>s (Type v) = {}" |
  "vars\<^sub>s (Sec s) = {}" |
  "vars\<^sub>s (SOp op a b) = (vars\<^sub>s a \<union> vars\<^sub>s b)" |
  "vars\<^sub>s (STer v s\<^sub>1 s\<^sub>2) = (vars\<^sub>p v \<union> vars\<^sub>s s\<^sub>1 \<union> vars\<^sub>s s\<^sub>2)"

primrec
  vars\<^sub>\<Gamma>\<^sub>e :: "('var, 'val, 'sec) exp \<Rightarrow> ('var) set" and
  vars\<^sub>\<Gamma>\<^sub>p :: "('var, 'val, 'sec) pred \<Rightarrow> ('var) set" and
  vars\<^sub>\<Gamma>\<^sub>s :: "('var, 'val, 'sec) secexp \<Rightarrow> ('var) set"
where
  "vars\<^sub>\<Gamma>\<^sub>e (Var v) = {}" |
  "vars\<^sub>\<Gamma>\<^sub>e (Const c) = {}" |
  "vars\<^sub>\<Gamma>\<^sub>e (Op bop e f) = (vars\<^sub>\<Gamma>\<^sub>e e \<union> vars\<^sub>\<Gamma>\<^sub>e f)" |
  "vars\<^sub>\<Gamma>\<^sub>e (Ternary v e\<^sub>1 e\<^sub>2) = (vars\<^sub>\<Gamma>\<^sub>p v \<union> vars\<^sub>\<Gamma>\<^sub>e e\<^sub>1 \<union> vars\<^sub>\<Gamma>\<^sub>e e\<^sub>2)" |
  "vars\<^sub>\<Gamma>\<^sub>p (Pb v) = {}" |
  "vars\<^sub>\<Gamma>\<^sub>p (PNeg p) = vars\<^sub>\<Gamma>\<^sub>p p" |
  "vars\<^sub>\<Gamma>\<^sub>p (PConj p q) = (vars\<^sub>\<Gamma>\<^sub>p p \<union> vars\<^sub>\<Gamma>\<^sub>p q)" |
  "vars\<^sub>\<Gamma>\<^sub>p (PDisj p q) = (vars\<^sub>\<Gamma>\<^sub>p p \<union> vars\<^sub>\<Gamma>\<^sub>p q)" |
  "vars\<^sub>\<Gamma>\<^sub>p (PImp p q) = (vars\<^sub>\<Gamma>\<^sub>p p \<union> vars\<^sub>\<Gamma>\<^sub>p q)" |
  "vars\<^sub>\<Gamma>\<^sub>p (PCmp cmp e f) = (vars\<^sub>\<Gamma>\<^sub>e e \<union> vars\<^sub>\<Gamma>\<^sub>e f)" |
  "vars\<^sub>\<Gamma>\<^sub>p (PSec cmp e f) = (vars\<^sub>\<Gamma>\<^sub>s e \<union> vars\<^sub>\<Gamma>\<^sub>s f)" |
  "vars\<^sub>\<Gamma>\<^sub>p (PAllVar f) = (\<Union>v. vars\<^sub>\<Gamma>\<^sub>p (f v))" |
  "vars\<^sub>\<Gamma>\<^sub>p (PAllSec f) = (\<Union>v. vars\<^sub>\<Gamma>\<^sub>p (f v))" |
  "vars\<^sub>\<Gamma>\<^sub>s (Type v) = {v}" |
  "vars\<^sub>\<Gamma>\<^sub>s (Sec s) = {}" |
  "vars\<^sub>\<Gamma>\<^sub>s (SOp op a b) = (vars\<^sub>\<Gamma>\<^sub>s a \<union> vars\<^sub>\<Gamma>\<^sub>s b)" |
  "vars\<^sub>\<Gamma>\<^sub>s (STer v s\<^sub>1 s\<^sub>2) = (vars\<^sub>\<Gamma>\<^sub>p v \<union> vars\<^sub>\<Gamma>\<^sub>s s\<^sub>1 \<union> vars\<^sub>\<Gamma>\<^sub>s s\<^sub>2)"

primrec
  subst\<^sub>e :: "('var,'val,'sec) exp \<Rightarrow> ('var) \<Rightarrow> ('var,'val,'sec) exp \<Rightarrow> ('var,'val,'sec) exp" and
  subst\<^sub>p :: "('var,'val,'sec) pred \<Rightarrow> ('var) \<Rightarrow> ('var,'val,'sec) exp \<Rightarrow> ('var,'val,'sec) pred" and
  subst\<^sub>s :: "('var,'val,'sec) secexp \<Rightarrow> ('var) \<Rightarrow> ('var,'val,'sec) exp \<Rightarrow> ('var,'val,'sec) secexp"
where
  "subst\<^sub>e (Var v) x e = (if v = x then e else Var v)" |
  "subst\<^sub>e (Const c) x e = (Const c)" |
  "subst\<^sub>e (Op bop f s) x e = (Op bop (subst\<^sub>e f x e) (subst\<^sub>e s x e))" |
  "subst\<^sub>e (Ternary v e\<^sub>1 e\<^sub>2) x e = (Ternary (subst\<^sub>p v x e) (subst\<^sub>e e\<^sub>1 x e) (subst\<^sub>e e\<^sub>2 x e))" |
  "subst\<^sub>p (Pb v) x e = (Pb v)" |
  "subst\<^sub>p (PNeg p) x e = (PNeg (subst\<^sub>p p x e))" |
  "subst\<^sub>p (PConj p q) x e = (PConj (subst\<^sub>p p x e) (subst\<^sub>p q x e))" |
  "subst\<^sub>p (PDisj p q) x e = (PDisj (subst\<^sub>p p x e) (subst\<^sub>p q x e))" |
  "subst\<^sub>p (PCmp cmp f s) x e = (PCmp cmp (subst\<^sub>e f x e) (subst\<^sub>e s x e))" |
  "subst\<^sub>p (PImp p q) x e = (PImp (subst\<^sub>p p x e) (subst\<^sub>p q x e))" |
  "subst\<^sub>p (PSec cmp f s) x e = (PSec cmp (subst\<^sub>s f x e) (subst\<^sub>s s x e))" |
  "subst\<^sub>p (PAllVar f) x e = (PAllVar (\<lambda>v. subst\<^sub>p (f v) x e))" |
  "subst\<^sub>p (PAllSec f) x e = (PAllSec (\<lambda>v. subst\<^sub>p (f v) x e))" |
  "subst\<^sub>s (Type v) x e = (Type v)" |     
  "subst\<^sub>s (Sec s) x e = (Sec s)" | 
  "subst\<^sub>s (SOp op a b) x e = (SOp op (subst\<^sub>s a x e) (subst\<^sub>s b x e))" |
  "subst\<^sub>s (STer v s\<^sub>1 s\<^sub>2) x e = (STer (subst\<^sub>p v x e) (subst\<^sub>s s\<^sub>1 x e) (subst\<^sub>s s\<^sub>2 x e))"
  
primrec
  subst\<^sub>\<Gamma>\<^sub>e :: "('var,'val,'sec) exp \<Rightarrow> ('var) \<Rightarrow> ('var,'val,'sec) secexp \<Rightarrow> ('var,'val,'sec) exp" and
  subst\<^sub>\<Gamma>\<^sub>p :: "('var,'val,'sec) pred \<Rightarrow> ('var) \<Rightarrow> ('var,'val,'sec) secexp \<Rightarrow> ('var,'val,'sec) pred" and
  subst\<^sub>\<Gamma>\<^sub>s :: "('var,'val,'sec) secexp \<Rightarrow> ('var) \<Rightarrow> ('var,'val,'sec) secexp \<Rightarrow> ('var,'val,'sec) secexp"
where
  "subst\<^sub>\<Gamma>\<^sub>e (Var v) x e = (Var v)" |
  "subst\<^sub>\<Gamma>\<^sub>e (Const c) x e = (Const c)" |
  "subst\<^sub>\<Gamma>\<^sub>e (Op bop f s) x e = (Op bop (subst\<^sub>\<Gamma>\<^sub>e f x e) (subst\<^sub>\<Gamma>\<^sub>e s x e))" |
  "subst\<^sub>\<Gamma>\<^sub>e (Ternary v e\<^sub>1 e\<^sub>2) x e = (Ternary (subst\<^sub>\<Gamma>\<^sub>p v x e) (subst\<^sub>\<Gamma>\<^sub>e e\<^sub>1 x e) (subst\<^sub>\<Gamma>\<^sub>e e\<^sub>2 x e))" |
  "subst\<^sub>\<Gamma>\<^sub>p (Pb v) x e = (Pb v)" |
  "subst\<^sub>\<Gamma>\<^sub>p (PNeg p) x e = (PNeg (subst\<^sub>\<Gamma>\<^sub>p p x e))" |
  "subst\<^sub>\<Gamma>\<^sub>p (PConj p q) x e = (PConj (subst\<^sub>\<Gamma>\<^sub>p p x e) (subst\<^sub>\<Gamma>\<^sub>p q x e))" |
  "subst\<^sub>\<Gamma>\<^sub>p (PDisj p q) x e = (PDisj (subst\<^sub>\<Gamma>\<^sub>p p x e) (subst\<^sub>\<Gamma>\<^sub>p q x e))" |
  "subst\<^sub>\<Gamma>\<^sub>p (PCmp cmp f s) x e = (PCmp cmp (subst\<^sub>\<Gamma>\<^sub>e f x e) (subst\<^sub>\<Gamma>\<^sub>e s x e))" |
  "subst\<^sub>\<Gamma>\<^sub>p (PImp p q) x e = (PImp (subst\<^sub>\<Gamma>\<^sub>p p x e) (subst\<^sub>\<Gamma>\<^sub>p q x e))" |
  "subst\<^sub>\<Gamma>\<^sub>p (PSec cmp f s) x e = (PSec cmp (subst\<^sub>\<Gamma>\<^sub>s f x e) (subst\<^sub>\<Gamma>\<^sub>s s x e))" |
  "subst\<^sub>\<Gamma>\<^sub>p (PAllVar f) x e = (PAllVar (\<lambda>v. subst\<^sub>\<Gamma>\<^sub>p (f v) x e))" |
  "subst\<^sub>\<Gamma>\<^sub>p (PAllSec f) x e = (PAllSec (\<lambda>v. subst\<^sub>\<Gamma>\<^sub>p (f v) x e))" |
  "subst\<^sub>\<Gamma>\<^sub>s (Type v) x e = (if (v = x) then e else (Type v))" |     
  "subst\<^sub>\<Gamma>\<^sub>s (Sec s) x e = (Sec s)" | 
  "subst\<^sub>\<Gamma>\<^sub>s (SOp op a b) x e = (SOp op (subst\<^sub>\<Gamma>\<^sub>s a x e) (subst\<^sub>\<Gamma>\<^sub>s b x e))" |
  "subst\<^sub>\<Gamma>\<^sub>s (STer v s\<^sub>1 s\<^sub>2) x e = (STer (subst\<^sub>\<Gamma>\<^sub>p v x e) (subst\<^sub>\<Gamma>\<^sub>s s\<^sub>1 x e) (subst\<^sub>\<Gamma>\<^sub>s s\<^sub>2 x e))"

primrec
  map\<^sub>e :: "('a \<Rightarrow> 'b) \<Rightarrow> ('a, 'val, 'sec) exp \<Rightarrow> ('b, 'val, 'sec) exp" and 
  map\<^sub>p :: "('a \<Rightarrow> 'b) \<Rightarrow> ('a, 'val, 'sec) pred \<Rightarrow> ('b, 'val, 'sec) pred" and
  map\<^sub>s :: "('a \<Rightarrow> 'b) \<Rightarrow> ('a, 'val, 'sec) secexp \<Rightarrow> ('b, 'val, 'sec) secexp"
where                                                     
  "map\<^sub>e f (Var p) = (Var (f p))" |
  "map\<^sub>e f (Const v) = (Const v)" |
  "map\<^sub>e f (Op c e g) = (Op c (map\<^sub>e f e) (map\<^sub>e f g))" |
  "map\<^sub>e f (Ternary v e\<^sub>1 e\<^sub>2) = (Ternary (map\<^sub>p f v) (map\<^sub>e f e\<^sub>1) (map\<^sub>e f e\<^sub>2))" |
  "map\<^sub>p f (Pb v) = (Pb v)" |
  "map\<^sub>p f (PNeg p) = PNeg (map\<^sub>p f p)" |
  "map\<^sub>p f (PConj p q) = (PConj (map\<^sub>p f p) (map\<^sub>p f q))" |
  "map\<^sub>p f (PDisj p q) = (PDisj (map\<^sub>p f p) (map\<^sub>p f q))" |
  "map\<^sub>p f (PCmp cmp e g) = (PCmp cmp (map\<^sub>e f e) (map\<^sub>e f g))" |
  "map\<^sub>p f (PImp p q) = (PImp (map\<^sub>p f p) (map\<^sub>p f q))" |
  "map\<^sub>p f (PSec cmp p q) = (PSec cmp (map\<^sub>s f p) (map\<^sub>s f q))" |
  "map\<^sub>p f (PAllVar q) = (PAllVar (\<lambda>v. map\<^sub>p f (q v)))" |
  "map\<^sub>p f (PAllSec q) = (PAllSec (\<lambda>v. map\<^sub>p f (q v)))" |
  "map\<^sub>s f (Type v) = (Type (f v))" |
  "map\<^sub>s f (Sec s) = (Sec s)" |
  "map\<^sub>s f (SOp c e g) = (SOp c (map\<^sub>s f e) (map\<^sub>s f g))" |
  "map\<^sub>s f (STer v s\<^sub>1 s\<^sub>2) = (STer (map\<^sub>p f v) (map\<^sub>s f s\<^sub>1) (map\<^sub>s f s\<^sub>2))"

abbreviation lower (infix "\<sqinter>" 60)
  where "lower a b \<equiv> SOp (inf) a b"

abbreviation upper (infix "\<squnion>" 60)
  where "upper a b \<equiv> SOp (sup) a b"

abbreviation higher (infix "\<sqsupseteq>" 62)
  where "higher a b \<equiv> PSec (\<ge>) a b"

definition entail :: "('Var,'Val,'Sec) pred \<Rightarrow> ('Var,'Val,'Sec) pred \<Rightarrow> bool"
  (infix "\<turnstile>\<^sub>p" 25)
  where "entail P P' \<equiv> \<forall>mem \<Gamma>. test mem \<Gamma> P \<longrightarrow> test mem \<Gamma> P'"

definition equiv :: "('Var,'Val,'Sec) pred \<Rightarrow> ('Var,'Val,'Sec) pred \<Rightarrow> bool"
  (infixl "=\<^sub>p" 50)
  where "equiv P P' \<equiv> \<forall>mem \<Gamma>. test mem \<Gamma> P = test mem \<Gamma> P'"

definition fold_conj :: "(_,_,_) pred list \<Rightarrow> (_,_,_) pred" 
  where "fold_conj l \<equiv> fold PConj l (Pb True)"

definition fold_conj\<^sub>s :: "(_,_,_) secexp list \<Rightarrow> (_,_,_) secexp"
  where "fold_conj\<^sub>s l \<equiv> fold (SOp (sup)) l (Sec bot)"

definition forVars :: "'a list \<Rightarrow> ('a \<Rightarrow> (_,_,_) pred) \<Rightarrow> (_,_,_) pred"
  where "forVars V f = fold_conj (map f V)"

definition forVars\<^sub>s :: "'a list \<Rightarrow> ('a \<Rightarrow> (_,_,_) secexp) \<Rightarrow> (_,_,_) secexp"
  where "forVars\<^sub>s V f = fold_conj\<^sub>s (map f V)"

lemma entail_refl [intro]:
  "P \<turnstile>\<^sub>p P"
  by (auto simp: entail_def)

lemma vars_det:
  "\<forall>v \<in> vars\<^sub>p p. m v = m' v \<Longrightarrow> \<forall>v \<in> vars\<^sub>\<Gamma>\<^sub>p p. \<Gamma> v = \<Gamma>' v \<Longrightarrow> test m \<Gamma> p = test m' \<Gamma>' p"
  "\<forall>v \<in> vars\<^sub>e e. m v = m' v \<Longrightarrow> \<forall>w \<in> vars\<^sub>\<Gamma>\<^sub>e e. \<Gamma> w = \<Gamma>' w \<Longrightarrow> eval m \<Gamma> e = eval m' \<Gamma>' e"
  "\<forall>v \<in> vars\<^sub>s s. m v = m' v \<Longrightarrow> \<forall>w \<in> vars\<^sub>\<Gamma>\<^sub>s s. \<Gamma> w = \<Gamma>' w \<Longrightarrow> eval\<^sub>s m \<Gamma> s = eval\<^sub>s m' \<Gamma>' s"
  by (induct rule: pred.induct exp.induct secexp.induct) auto

lemma map_vars_det:
  "\<forall>v \<in> vars\<^sub>p P. m v = m' (f v) \<Longrightarrow> \<forall>v \<in> vars\<^sub>\<Gamma>\<^sub>p P. \<Gamma> v = \<Gamma>' (f v) \<Longrightarrow> test m \<Gamma> P = test m' \<Gamma>' ((map\<^sub>p f (P :: ('a, 'b, 'c) pred)))"
  "\<forall>v \<in> vars\<^sub>e e. m v = m' (f v) \<Longrightarrow> \<forall>v \<in> vars\<^sub>\<Gamma>\<^sub>e e. \<Gamma> v = \<Gamma>' (f v) \<Longrightarrow> eval m \<Gamma> e = eval m' \<Gamma>' ((map\<^sub>e f (e :: ('a, 'b, 'c) exp)))"
  "\<forall>v \<in> vars\<^sub>s s. m v = m' (f v) \<Longrightarrow> \<forall>v \<in> vars\<^sub>\<Gamma>\<^sub>s s. \<Gamma> v = \<Gamma>' (f v) \<Longrightarrow> eval\<^sub>s m \<Gamma> s = eval\<^sub>s m' \<Gamma>' ((map\<^sub>s f (s :: ('a, 'b, 'c) secexp)))"
  by (induct P and e rule: pred.induct exp.induct secexp.induct) auto

subsection \<open> subste \<close>

lemma subst_mem_upd: 
  assumes "(\<And>y. y \<noteq> x \<Longrightarrow> mem' y = mem y)"
  assumes "eval mem' \<Gamma> e = mem x"
  shows
  "eval mem' \<Gamma> (subst\<^sub>e f x e) = eval mem \<Gamma> f" 
  "test mem' \<Gamma> (subst\<^sub>p P x e) = test mem \<Gamma> P"
  "eval\<^sub>s mem' \<Gamma> (subst\<^sub>s s x e) = eval\<^sub>s mem \<Gamma> s"
  using assms 
  by (induct f and P and s rule: exp.induct pred.induct secexp.induct) auto

lemma subst_nop [simp]:
  "x \<notin> vars\<^sub>p P \<Longrightarrow> subst\<^sub>p P x v = (P :: ('a,'b,'c) pred)" 
  "x \<notin> vars\<^sub>e f \<Longrightarrow> subst\<^sub>e f x v = (f :: ('a,'b,'c) exp)"
  "x \<notin> vars\<^sub>s s \<Longrightarrow> subst\<^sub>s s x v = (s :: ('a,'b,'c) secexp)"
  by (induct P and f rule: pred.induct exp.induct secexp.induct) auto

lemma subst_vars [simp]:
  "vars\<^sub>e (subst\<^sub>e (f :: ('a,'b,'c) exp) x e) = vars\<^sub>e f - {x} \<union> (if x \<in> vars\<^sub>e f then vars\<^sub>e e else {})"
  "vars\<^sub>p (subst\<^sub>p (P :: ('a,'b,'c) pred) x e) = vars\<^sub>p P - {x} \<union> (if x \<in> vars\<^sub>p P then vars\<^sub>e e else {})"
  "vars\<^sub>s (subst\<^sub>s (s :: ('a,'b,'c) secexp) x e) = vars\<^sub>s s - {x} \<union> (if x \<in> vars\<^sub>s s then vars\<^sub>e e else {})"
proof (induct f and P and s rule: exp.induct pred.induct secexp.induct)
  case (PAllVar f)
  hence "(\<Union>xa. vars\<^sub>p (subst\<^sub>p (f xa) x e)) = (\<Union>xa. vars\<^sub>p (f xa)) - {( x)} \<union> (\<Union>xa. (if ( x) \<in> vars\<^sub>p (f xa) then vars\<^sub>e e else {}))"
    by blast
  thus ?case by simp
next
  case (PAllSec f)
  hence "(\<Union>xa. vars\<^sub>p (subst\<^sub>p (f xa) x e)) = ((\<Union>xa. vars\<^sub>p (f xa) - {( x)}) \<union> (\<Union>xa. (if ( x) \<in> vars\<^sub>p (f xa) then vars\<^sub>e e else {})))"
    by blast
  then show ?case by simp
qed (auto)

lemma subst_vars\<^sub>\<Gamma> [simp]:
  "vars\<^sub>\<Gamma>\<^sub>e (subst\<^sub>e (f :: ('a,'b,'c) exp) x v) = vars\<^sub>\<Gamma>\<^sub>e f \<union> (if x \<in> vars\<^sub>e f then vars\<^sub>\<Gamma>\<^sub>e v else {})"
  "vars\<^sub>\<Gamma>\<^sub>p (subst\<^sub>p (P :: ('a,'b,'c) pred) x v) = vars\<^sub>\<Gamma>\<^sub>p P \<union> (if x \<in> vars\<^sub>p P then vars\<^sub>\<Gamma>\<^sub>e v else {})"
  "vars\<^sub>\<Gamma>\<^sub>s (subst\<^sub>s (s :: ('a,'b,'c) secexp) x v) = vars\<^sub>\<Gamma>\<^sub>s s \<union> (if x \<in> vars\<^sub>s s then vars\<^sub>\<Gamma>\<^sub>e v else {})"
proof(induct f and P and s rule: exp.induct pred.induct secexp.induct)
  case (PAllVar f)
  hence "(\<Union>xa. vars\<^sub>\<Gamma>\<^sub>p (subst\<^sub>p (f xa) x v)) = (\<Union>xa. vars\<^sub>\<Gamma>\<^sub>p (f xa)) \<union> (\<Union>xa. (if ( x) \<in> vars\<^sub>p (f xa) then vars\<^sub>\<Gamma>\<^sub>e v else {}))"
    by blast
  thus ?case by simp
next
  case (PAllSec f)
  hence "(\<Union>xa. vars\<^sub>\<Gamma>\<^sub>p (subst\<^sub>p (f xa) x v)) = (\<Union>xa. vars\<^sub>\<Gamma>\<^sub>p (f xa)) \<union> (\<Union>xa. (if ( x) \<in> vars\<^sub>p (f xa) then vars\<^sub>\<Gamma>\<^sub>e v else {}))"
    by blast
  thus ?case by simp
qed auto

subsection \<open> subst\<Gamma> \<close>

lemma subst\<^sub>\<Gamma>_mem_upd:
  assumes "\<And>y. y \<noteq> x \<Longrightarrow> \<Gamma>' y = \<Gamma> y"
  assumes "eval\<^sub>s mem \<Gamma>' e = \<Gamma> x"
  shows 
    "test mem \<Gamma>' (subst\<^sub>\<Gamma>\<^sub>p P x e) = test mem \<Gamma> P"
    "eval mem \<Gamma>' (subst\<^sub>\<Gamma>\<^sub>e f x e) = eval mem \<Gamma> f"
    "eval\<^sub>s mem \<Gamma>' (subst\<^sub>\<Gamma>\<^sub>s s x e) = eval\<^sub>s mem \<Gamma> s"
  using assms 
  by (induct P and f and s rule: pred.induct exp.induct secexp.induct) auto

lemma subst\<^sub>\<Gamma>_nop [simp]:
  "x \<notin> vars\<^sub>\<Gamma>\<^sub>p P \<Longrightarrow> subst\<^sub>\<Gamma>\<^sub>p P x v = (P :: ('a,'b,'c) pred)" 
  "x \<notin> vars\<^sub>\<Gamma>\<^sub>e f \<Longrightarrow> subst\<^sub>\<Gamma>\<^sub>e f x v = (f :: ('a,'b,'c) exp)"
  "x \<notin> vars\<^sub>\<Gamma>\<^sub>s s \<Longrightarrow> subst\<^sub>\<Gamma>\<^sub>s s x v = (s :: ('a,'b,'c) secexp)"
  by (induct P and f and s rule: pred.induct exp.induct secexp.induct) auto

lemma subst\<^sub>\<Gamma>_vars\<^sub>\<Gamma> [simp]:
  "vars\<^sub>\<Gamma>\<^sub>p (subst\<^sub>\<Gamma>\<^sub>p P x v) = vars\<^sub>\<Gamma>\<^sub>p P - {x} \<union> (if x \<in> vars\<^sub>\<Gamma>\<^sub>p P then vars\<^sub>\<Gamma>\<^sub>s v else {})"
  "vars\<^sub>\<Gamma>\<^sub>e (subst\<^sub>\<Gamma>\<^sub>e f x v) = vars\<^sub>\<Gamma>\<^sub>e f - {x} \<union> (if x \<in> vars\<^sub>\<Gamma>\<^sub>e f then vars\<^sub>\<Gamma>\<^sub>s v else {})"
  "vars\<^sub>\<Gamma>\<^sub>s (subst\<^sub>\<Gamma>\<^sub>s s x v) = vars\<^sub>\<Gamma>\<^sub>s s - {x} \<union> (if x \<in> vars\<^sub>\<Gamma>\<^sub>s s then vars\<^sub>\<Gamma>\<^sub>s v else {})"
proof (induct P and f and s rule: pred.induct exp.induct secexp.induct)
  case (PAllVar f)
  hence "(\<Union>xa. vars\<^sub>\<Gamma>\<^sub>p (subst\<^sub>\<Gamma>\<^sub>p (f xa) x v)) = (\<Union>xa. vars\<^sub>\<Gamma>\<^sub>p (f xa) - {( x)}) \<union> (\<Union>xa. (if ( x) \<in> vars\<^sub>\<Gamma>\<^sub>p (f xa) then vars\<^sub>\<Gamma>\<^sub>s v else {}))"
    by blast
  then show ?case by simp
next
  case (PAllSec f)
  hence "(\<Union>xa. vars\<^sub>\<Gamma>\<^sub>p (subst\<^sub>\<Gamma>\<^sub>p (f xa) x v)) = (\<Union>xa. vars\<^sub>\<Gamma>\<^sub>p (f xa) - {( x)}) \<union> (\<Union>xa. (if ( x) \<in> vars\<^sub>\<Gamma>\<^sub>p (f xa) then vars\<^sub>\<Gamma>\<^sub>s v else {}))"
    by blast
  then show ?case by simp
qed (auto)

lemma subst\<^sub>\<Gamma>_vars [simp]:
  "vars\<^sub>e (subst\<^sub>\<Gamma>\<^sub>e f x v) = vars\<^sub>e f \<union> (if x \<in> vars\<^sub>\<Gamma>\<^sub>e f then vars\<^sub>s v else {})"
  "vars\<^sub>p (subst\<^sub>\<Gamma>\<^sub>p P x v) = vars\<^sub>p P \<union> (if x \<in> vars\<^sub>\<Gamma>\<^sub>p P then vars\<^sub>s v else {})"
  "vars\<^sub>s (subst\<^sub>\<Gamma>\<^sub>s s x v) = vars\<^sub>s s \<union> (if x \<in> vars\<^sub>\<Gamma>\<^sub>s s then vars\<^sub>s v else {})"
proof (induct f and P and s rule: exp.induct pred.induct secexp.induct)
  case (PAllVar f)
  hence "(\<Union>xa. vars\<^sub>p (subst\<^sub>\<Gamma>\<^sub>p (f xa) x v)) = (\<Union>xa. vars\<^sub>p (f xa)) \<union> (\<Union>xa. (if ( x) \<in> vars\<^sub>\<Gamma>\<^sub>p (f xa) then vars\<^sub>s v else {}))"
    by blast
  then show ?case by simp
next
case (PAllSec f)
  hence "(\<Union>xa. vars\<^sub>p (subst\<^sub>\<Gamma>\<^sub>p (f xa) x v)) = (\<Union>xa. vars\<^sub>p (f xa)) \<union> (\<Union>xa. (if ( x) \<in> vars\<^sub>\<Gamma>\<^sub>p (f xa) then vars\<^sub>s v else {}))"
    by blast
  then show ?case by simp
qed (auto)

lemma vars_map [simp]:
  "vars\<^sub>e (map\<^sub>e f e) = f ` vars\<^sub>e (e :: ('a,'b,'c) exp)"
  "vars\<^sub>p (map\<^sub>p f p) = f ` vars\<^sub>p (p :: ('a,'b,'c) pred)"
  "vars\<^sub>s (map\<^sub>s f s) = f ` vars\<^sub>s (s :: ('a,'b,'c) secexp)"                           
  by (induct e and p and s rule: exp.induct pred.induct secexp.induct) auto

lemma vars_map\<^sub>\<Gamma> [simp]:
  "vars\<^sub>\<Gamma>\<^sub>e (map\<^sub>e f e) = f ` vars\<^sub>\<Gamma>\<^sub>e (e :: ('a,'b,'c) exp)"
  "vars\<^sub>\<Gamma>\<^sub>p (map\<^sub>p f p) = f ` vars\<^sub>\<Gamma>\<^sub>p (p :: ('a,'b,'c) pred)"
  "vars\<^sub>\<Gamma>\<^sub>s (map\<^sub>s f s) = f ` vars\<^sub>\<Gamma>\<^sub>s (s :: ('a,'b,'c) secexp)"
  by (induct e and p and s rule: exp.induct pred.induct secexp.induct) auto

lemma entail_trans[trans]:
  "P \<turnstile>\<^sub>p Q \<Longrightarrow> Q \<turnstile>\<^sub>p R \<Longrightarrow> P \<turnstile>\<^sub>p R"
  by (auto simp: entail_def)

lemma entail_conj1[intro]:
  "P \<and>\<^sub>p Q \<turnstile>\<^sub>p Q"
  by (auto simp: entail_def)

lemma entail_conj2[intro]:
  "P \<and>\<^sub>p Q \<turnstile>\<^sub>p P"
  by (auto simp: entail_def)

lemma test_fold:
  "test e \<Gamma> (fold (\<and>\<^sub>p) (map f L) P) = ((\<forall>v \<in> set L. test e \<Gamma> (f v)) \<and> test e \<Gamma> P)"
  by (induct L arbitrary: P, auto)

lemma test_low [simp]:
  "test e \<Gamma> (forVars (V :: 'a list) f) = (\<forall>v \<in> set V. test e \<Gamma> (f v))"
  using test_fold[of e \<Gamma> f] by (auto simp: forVars_def fold_conj_def)

lemma eval\<^sub>s_fold_sup[simp]:
  "eval\<^sub>s m (\<Gamma>:: 'b \<Rightarrow> 'a::bounded_lattice) (fold (SOp sup) V P) = (fold sup (map (\<lambda>v. eval\<^sub>s m \<Gamma> v) V) (eval\<^sub>s m \<Gamma> P))"
  by (induct V arbitrary: P) simp+

lemma forVars\<^sub>s [simp]:
  "eval\<^sub>s m (\<Gamma>:: 'b \<Rightarrow> 'a::bounded_lattice) (forVars\<^sub>s V f) = supl (map (\<lambda>v. eval\<^sub>s m \<Gamma> (f v)) V)"
proof -
  have [simp]: "(map (\<lambda>v. eval\<^sub>s m \<Gamma> (f v)) V) = (map (eval\<^sub>s m \<Gamma>) (map f V))" by simp
  show ?thesis unfolding forVars\<^sub>s_def fold_conj\<^sub>s_def supl_def by simp
qed

lemma test_subst\<^sub>\<Gamma> [simp]:
  "eval\<^sub>s m \<Gamma> (subst\<^sub>\<Gamma>\<^sub>s P y Q) = eval\<^sub>s m (\<Gamma> (y := eval\<^sub>s m \<Gamma> Q)) P"
  by (intro subst\<^sub>\<Gamma>_mem_upd, auto)

lemma test_subst\<^sub>p [simp]:
  "test m \<Gamma> (subst\<^sub>p P y e) = test (m (y := eval m \<Gamma> e)) \<Gamma> P"
  by (intro subst_mem_upd, auto)

lemma [simp]:
  "eval\<^sub>s m \<Gamma> (subst\<^sub>s y x e) = eval\<^sub>s (m(x := eval m \<Gamma> e)) \<Gamma> y"
  by (simp add: subst_mem_upd(3))

lemma test_subst\<^sub>\<Gamma>\<^sub>p [simp]:
  "test m \<Gamma> (subst\<^sub>\<Gamma>\<^sub>p Q x e) = test m (\<Gamma>(x := eval\<^sub>s m \<Gamma> e)) Q"
  by (intro subst\<^sub>\<Gamma>_mem_upd, auto)

lemma vars_const [elim]:
  assumes "vars\<^sub>p Q = {}" "vars\<^sub>\<Gamma>\<^sub>p Q = {}"
  obtains (T) "Q =\<^sub>p Pb True" | (F) "Q =\<^sub>p Pb False"
proof (cases "\<exists>m \<Gamma>. test m \<Gamma> Q")
  case True
  then obtain m' \<Gamma>' where t: "test m' \<Gamma>' Q" by blast
  have "\<forall>m \<Gamma>. test m \<Gamma> Q"
  proof (intro allI)
    fix m \<Gamma>
    have "test m \<Gamma> Q = test m' \<Gamma>' Q" using assms by (auto intro!: vars_det(1))
    thus "test m \<Gamma> Q" using t by auto
  qed
  then show ?thesis using that by (auto simp: equiv_def)
next
  case False
  then show ?thesis using that by (auto simp: equiv_def)
qed

lemma equiv_trans [trans]:
  shows "P1 =\<^sub>p P2 \<Longrightarrow> P2 =\<^sub>p P3 \<Longrightarrow> P1 =\<^sub>p P3"
  unfolding equiv_def by metis

lemma entail_conj [intro]:
  "A \<turnstile>\<^sub>p B \<Longrightarrow> A \<turnstile>\<^sub>p C \<Longrightarrow> A \<turnstile>\<^sub>p B \<and>\<^sub>p C"
  by (auto simp: entail_def)

lemma entail_imp [intro]:
  "A \<and>\<^sub>p B \<turnstile>\<^sub>p C \<Longrightarrow> A \<turnstile>\<^sub>p (B \<longrightarrow>\<^sub>p C)"
  by (auto simp: entail_def)

lemma entail_true [intro]:
  "A \<turnstile>\<^sub>p Pb True"
  by (auto simp: entail_def)

lemma entail_imp_pres[intro]:
  "A \<turnstile>\<^sub>p B \<Longrightarrow> (C \<longrightarrow>\<^sub>p A) \<turnstile>\<^sub>p (C \<longrightarrow>\<^sub>p B)" 
  by (auto simp: entail_def)

primrec foldi :: "nat \<Rightarrow> (nat \<Rightarrow> 'a \<Rightarrow> 'b \<Rightarrow> 'b) \<Rightarrow> 'a list \<Rightarrow> 'b \<Rightarrow> 'b" where
  fold_Nil:  "foldi n f [] = id" |
  fold_Cons: "foldi n f (x # xs) = foldi (Suc n) f xs \<circ> f n x"

definition index_assert
  where "index_assert x i P \<equiv> foldi 0 (\<lambda>j v Q. Q \<and>\<^sub>p (PCmp (=) i (Const j) \<longrightarrow>\<^sub>p P v)) x (Pb True)"

definition list\<^sub>e
  where "list\<^sub>e x i \<equiv> foldi 0 (\<lambda>j v. Ternary (PCmp (=) i (Const j)) (Var v)) x (Const 0)"

definition list\<^sub>s :: "'c list \<Rightarrow> ('a, nat, 'b) exp \<Rightarrow> ('c \<Rightarrow> ('a, nat, 'b) secexp) \<Rightarrow> ('a, nat, 'b::bounded_lattice) secexp"
  where "list\<^sub>s x i f \<equiv> foldi 0 (\<lambda>j v. STer (PCmp (=) i (Const j)) (f v)) x (Sec top)"

text \<open>Show that liste is equivalent to evaluating a list read for values\<close>
lemma eval_foldi_nop [simp]:
  assumes "eval m \<Gamma> i < k"
  shows "eval m \<Gamma> (foldi k (\<lambda>y v. Ternary (PCmp (=) i (Const y)) (Q v)) x q) = eval m \<Gamma> q"
  using assms
proof (induct x arbitrary: k q)
  case (Cons a x)
  thus ?case using Cons(1)[of "Suc k" "Ternary (PCmp (=) (Const k) i) (Q a) q"] by simp
qed simp

lemma list\<^sub>e [simp]:
  assumes "eval m \<Gamma> i < length x"
  shows "eval m \<Gamma> (list\<^sub>e x i) = m (x ! eval m \<Gamma> i)"
proof -
  have "\<And>k Q. k \<le> eval m \<Gamma> i \<Longrightarrow> eval m \<Gamma> i - k < length x \<Longrightarrow> eval m \<Gamma> (foldi k (\<lambda>j v. Ternary (PCmp (=) i (Const j)) (Var v)) x Q) = m (x ! (eval m \<Gamma> i - k))"
  proof (induct x)
    case (Cons a x)
    then show ?case 
    proof (cases "eval m \<Gamma> i = k")
      case True
      then show ?thesis by simp
    next
      case False
      have "eval m \<Gamma> (foldi (Suc k) (\<lambda>j v. Ternary (PCmp (=) i (Const j)) (Var v)) x (Ternary (PCmp (=) i (Const k)) (Var a) Q)) = m (x ! (eval m \<Gamma> i - Suc k))"
        using False Cons(2,3) by (intro Cons(1)) auto
      thus ?thesis using False Cons(2) by simp
    qed 
  qed simp
  thus ?thesis using assms 
    by (auto simp: list\<^sub>e_def)
qed

text \<open>Show that liste is equivalent to evaluating a list read for values\<close>
lemma eval\<^sub>s_foldi_nop [simp]:
  assumes "eval m \<Gamma> i < k"
  shows "eval\<^sub>s m \<Gamma> (foldi k (\<lambda>y v. STer (PCmp (=) i (Const y)) (Q v)) x q) = eval\<^sub>s m \<Gamma> q"
  using assms
proof (induct x arbitrary: k q)
  case (Cons a x)
  thus ?case using Cons(1)[of "Suc k" "STer (PCmp (=) (Const k) i) (Q a) q"] by simp
qed simp

lemma list\<^sub>s [simp]:
  assumes "eval m \<Gamma> i < length x"
  shows "eval\<^sub>s m \<Gamma> (list\<^sub>s x i f) = eval\<^sub>s m \<Gamma> (f (x ! eval m \<Gamma> i))"
proof -
  have "\<And>k Q. k \<le> eval m \<Gamma> i \<Longrightarrow> eval m \<Gamma> i - k < length x \<Longrightarrow> eval\<^sub>s m \<Gamma> (foldi k (\<lambda>j v. STer (PCmp (=) i (Const j)) (f v)) x Q) = eval\<^sub>s m \<Gamma> (f (x ! (eval m \<Gamma> i - k)))"
  proof (induct x)
    case (Cons a x)
    then show ?case 
    proof (cases "eval m \<Gamma> i = k")
      case True
      then show ?thesis by simp
    next
      case False
      have "eval\<^sub>s m \<Gamma> (foldi (Suc k) (\<lambda>j v. STer (PCmp (=) i (Const j)) (f v)) x (STer (PCmp (=) i (Const k)) (f a) Q)) = eval\<^sub>s m \<Gamma> (f  (x ! (eval m \<Gamma> i - Suc k)))"
        using False Cons(2,3) by (intro Cons(1)) auto
      thus ?thesis using False Cons(2) by simp
    qed 
  qed simp
  thus ?thesis using assms 
    by (auto simp: list\<^sub>s_def)
qed

text \<open>Show that index_assert is equivalent to asserting a property for a particular index\<close>
lemma test_foldi_nop_conj [simp]:
  assumes "eval m \<Gamma> i < k"
  shows "test m \<Gamma> (foldi ( k) (\<lambda>j v Q. Q \<and>\<^sub>p (PCmp (=) i (Const j) \<longrightarrow>\<^sub>p P v)) x Q) = test m \<Gamma> Q"
  using assms by (induct x arbitrary: k Q) auto

lemma test_index [simp]:
  assumes "eval m \<Gamma> i < length x"
  shows "test m \<Gamma> (index_assert x i P) = test m \<Gamma> (P (x ! eval m \<Gamma> i))"
proof -
  have "\<And>k Q. k \<le> eval m \<Gamma> i \<Longrightarrow> eval m \<Gamma> i - k < length x \<Longrightarrow> test m \<Gamma> (foldi k (\<lambda>j v Q. Q \<and>\<^sub>p (PCmp (=) i (Const j) \<longrightarrow>\<^sub>p P v)) x Q) = test m \<Gamma> (P (x ! (eval m \<Gamma> i - k)) \<and>\<^sub>p Q)"
  proof (induct x)
    case (Cons a x)
    then show ?case
    proof (cases "eval m \<Gamma> i = k")
      case False
      have "test m \<Gamma> (foldi (Suc k) (\<lambda>j v Q. Q \<and>\<^sub>p (PCmp (=) i (Const j) \<longrightarrow>\<^sub>p P v)) x (Q \<and>\<^sub>p (PCmp (=) i (Const k) \<longrightarrow>\<^sub>p P a))) = 
            test m \<Gamma> (P (x ! (eval m \<Gamma> i - (Suc k))) \<and>\<^sub>p (Q \<and>\<^sub>p (PCmp (=) i (Const k) \<longrightarrow>\<^sub>p P a)))"
        using False Cons(2,3) by (intro Cons(1)) auto
      then show ?thesis using False Cons(2) by simp
    qed auto
  qed simp
  thus ?thesis using assms by (auto simp: index_assert_def)
qed

definition pall_var :: "'var  \<Rightarrow> ('var,'val,'sec) pred \<Rightarrow> ('var,'val,'sec) pred"
  where "pall_var x P \<equiv> PAllVar (\<lambda>v. PAllSec (\<lambda>s. subst\<^sub>p (subst\<^sub>\<Gamma>\<^sub>p P x (Sec s)) x (Const v)))"

definition pall :: "'var list \<Rightarrow> ('var,'val,'sec) pred \<Rightarrow> ('var,'val,'sec) pred"
  where "pall xs P \<equiv> fold pall_var xs P"

lemma [simp]:
  "test m \<Gamma> (pall_var x P) \<equiv> \<forall>v s. test (m(x := v)) (\<Gamma>(x := s)) P"
  unfolding pall_var_def by auto

lemma [simp]:
  "test m \<Gamma> (fold pall_var xs P) = (\<forall>m' \<Gamma>'. test (override_on m m' (set xs)) (override_on \<Gamma> \<Gamma>' (set xs)) P)"
proof (induct xs arbitrary: P)
  case Nil
  then show ?case by auto
next
  case (Cons a xs)
  show ?case
    apply (simp add: Cons[of "pall_var a P"])
  proof (auto)
    fix m' \<Gamma>'
    assume "\<forall>m' \<Gamma>' v s.
          test
           ((override_on m m' (set xs))(a := v))
           ((override_on \<Gamma> \<Gamma>' (set xs))(a := s))
           P"
    thus "test
        (override_on m m' (insert a (set xs)))
        (override_on \<Gamma> \<Gamma>' (insert a (set xs))) P"
      unfolding override_on_insert by blast
  next
    fix m' \<Gamma>' v s
    assume "\<forall>m' \<Gamma>'.
          test
           (override_on m m' (insert a (set xs)))
           (override_on \<Gamma> \<Gamma>' (insert a (set xs)))
           P"
    hence "test
           (override_on m (m'(a := v)) (insert a (set xs)))
           (override_on \<Gamma> (\<Gamma>'(a := s)) (insert a (set xs)))
           P"
      by auto
    moreover have "((override_on m (m'(a := v)) (set xs))
      (a := v)) = ((override_on m m' (set xs))(a := v))"
      by (auto simp: override_on_def)
    moreover have "((override_on \<Gamma> (\<Gamma>'(a := s)) (set xs))
      (a := s)) = ((override_on \<Gamma> \<Gamma>' (set xs))(a := s))"
      by (auto simp: override_on_def)
    ultimately show "test ((override_on m m' (set xs))(a := v))
        ((override_on \<Gamma> \<Gamma>' (set xs))(a := s)) P"
    unfolding override_on_insert
    by simp
qed
qed

lemma [simp]:
  "test m \<Gamma> (pall xs P) \<equiv> \<forall>m' \<Gamma>'. test (override_on m m' (set xs)) (override_on \<Gamma> \<Gamma>' (set xs)) P"
  unfolding pall_def by auto

end