theory Basic_Lattice
  imports Main
begin
datatype sec = high | low

instantiation sec :: bounded_lattice
begin

definition "bot_sec \<equiv> low"
definition "top_sec \<equiv> high"
definition "sup_sec \<equiv> \<lambda>x y. if x = low \<and> y = low then low else high"
definition "inf_sec \<equiv> \<lambda>x y. if x = high \<and> y = high then high else low"
definition "less_eq_sec \<equiv> \<lambda>x y. if x = high \<and> y = low then False else True"
definition "less_sec \<equiv> \<lambda>x y. if x = low \<and> y = high then True else False"

instance 
  apply standard
  apply (auto simp: less_sec_def less_eq_sec_def bot_sec_def top_sec_def sup_sec_def inf_sec_def split: if_splits sec.splits) 
  apply (case_tac y,  auto)
  apply (case_tac x, auto)
  apply (case_tac y, auto)
  apply (case_tac x, auto)
  apply (case_tac y, auto)
  apply (case_tac x, auto)
  apply (case_tac y, auto)
  apply (case_tac z, case_tac y, auto)
  apply (case_tac z, case_tac y, auto)
  apply (case_tac y, case_tac z, auto)
  apply (case_tac z, case_tac y, auto)
  done
end

end