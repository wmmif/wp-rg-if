theory WP_Typed_Rely_Guarantee
  imports Typed_Bisimulation Language_Lift Primed_Typed_Predicate_Language "HOL-Eisbach.Eisbach"
begin

text \<open>
Combine the deeply embedded predicate language with the abstract typed rely guarantee information
flow logic to generate an 'executable' version, based on the weakest-pre rules found in the paper.
\<close>

section \<open>Language Definition\<close>

type_synonym ('Var,'Sexp) Policy = "'Var \<Rightarrow> 'Sexp"

datatype ('Var,'Val,'Sec,'Aexp,'Bexp) WPLang =
  Skip
| Act "('Var,'Aexp,'Bexp) Action"
| Seq "('Var,'Val,'Sec,'Aexp,'Bexp) WPLang" "('Var,'Val,'Sec,'Aexp,'Bexp) WPLang" ("(_;/ _)"      [60,61] 60)
| If "('Var,'Aexp,'Bexp) Action" "('Var,'Val,'Sec,'Aexp,'Bexp) WPLang" "('Var,'Val,'Sec,'Aexp,'Bexp) WPLang" ("(1IF _/ THEN _ / ELSE _/ FI)"  [0,0,0] 61)
| While "('Var,'Aexp,'Bexp) Action" "('Var,'Val,'Sec) pred"  "('Var,'Val,'Sec,'Aexp,'Bexp) WPLang" ("(1WHILE _/ INV {_} //DO _ /OD)"  [0,0,0] 61)
| DoWhile "('Var,'Val,'Sec,'Aexp,'Bexp) WPLang" "('Var,'Val,'Sec) pred" "('Var,'Aexp,'Bexp) Action" ("(1DO _/ INV {_} //WHILE _ )"  [0,0,100] 61)
| TryUntil "('Var,'Val,'Sec) pred" "('Var,'Val,'Sec,'Aexp,'Bexp) WPLang" "('Var,'Aexp,'Bexp) Action" ("(1TRY {_} _ //UNTIL _ )"  [0,0,100] 61)

section \<open>Initial Implementation Definitions\<close>
text \<open>
  Introduce sufficient definitions and lemmas to establish the sublocale 
  with the abstract type system.
  Currently this is only requires the concept of a information flow update,
  which describes how the logic updates \<Gamma> entries due to an action.
\<close>
locale type_rg_if_impl = 
  language_lift eval\<^sub>A eval\<^sub>B eval\<^sub>C lift\<^sub>A lift\<^sub>B lift\<^sub>C vars\<^sub>A vars\<^sub>B vars\<^sub>C not
  for eval\<^sub>A :: "('Var,nat) Mem \<Rightarrow> 'Aexp \<Rightarrow> nat"
  and eval\<^sub>B :: "('Var,nat) Mem \<Rightarrow> 'Bexp \<Rightarrow> bool"
  and eval\<^sub>C :: "('Var,nat) Mem \<Rightarrow> 'Sexp \<Rightarrow> 'Sec::bounded_lattice"
  and lift\<^sub>A :: "'Aexp \<Rightarrow> ('Var,nat,'Sec) exp"
  and lift\<^sub>B :: "'Bexp \<Rightarrow> ('Var,nat,'Sec) pred"
  and lift\<^sub>C :: "'Sexp \<Rightarrow> ('Var,nat,'Sec) secexp"
  and vars\<^sub>A :: "'Aexp \<Rightarrow> ('Var) list"
  and vars\<^sub>B :: "'Bexp \<Rightarrow> ('Var) list"
  and vars\<^sub>C :: "'Sexp \<Rightarrow> ('Var) list"
  and not :: "'Bexp \<Rightarrow> 'Bexp" +
  fixes all_vars :: "'Var list"
  assumes all_vars_correct[simp]: "set all_vars = UNIV"

context type_rg_if_impl
begin

text \<open>Define how the type context is updated for each action\<close>
fun flow :: "(('Var,nat) Mem,'Sec::bounded_lattice) TType \<Rightarrow> ('Var,'Aexp,'Bexp) Action \<Rightarrow> ('Var,nat) Mem \<Rightarrow> ('Var,'Sec) Ctx rel"   
  where
    "flow s (x \<leftarrow> e) m = {(\<Gamma>,\<Gamma>( x := supl (map \<Gamma> (vars\<^sub>A e)))) | \<Gamma>. True}" |
    "flow s (CAS x e\<^sub>1 e\<^sub>2) m = {(\<Gamma>,\<Gamma>( x := supl (map \<Gamma> (vars\<^sub>A e\<^sub>2)))) | \<Gamma>. True}" |
    "flow s (Store a i e) m = {(\<Gamma>,\<Gamma>( a ! (eval\<^sub>A m i) := supl (map \<Gamma> (vars\<^sub>A e)))) | \<Gamma>. supl (map \<Gamma> (vars\<^sub>A i)) \<le> s m }" |
    "flow s (Load r a i) m = {(\<Gamma>,\<Gamma>(r := \<Gamma> (a ! eval\<^sub>A m i))) | \<Gamma>. supl (map \<Gamma> (vars\<^sub>A i)) \<le> s m }" |
    "flow _ _ _ = {(\<Gamma>,\<Gamma>) | \<Gamma>. True}"

definition \<S>
  where "\<S> i \<equiv> {(m\<^sub>1,m\<^sub>2). m\<^sub>1 i = m\<^sub>2 i}"

lemma [simp]:
  "fold sup (map \<Gamma> l) (b :: 'Sec) \<le> s = (b \<le> s \<and> (\<forall>v \<in> set l. \<Gamma> v \<le> s))"
  by (induct l arbitrary: b; auto)

lemma [simp]:
  "top \<le> (b :: 'Sec) = (top = b)"
  using top.extremum_uniqueI by auto

text \<open>Show that the type context update is sound\<close>
lemma flow_sound_impl:
  "(m\<^sub>1,m\<^sub>1') \<in> update \<alpha> \<longrightarrow> (m\<^sub>2,m\<^sub>2') \<in> update \<alpha> \<longrightarrow> (\<Gamma>,\<Gamma>') \<in> flow s \<alpha> m\<^sub>1 \<longrightarrow> s m\<^sub>1 \<ge> s m\<^sub>1' \<longrightarrow> s m\<^sub>2 \<ge> s m\<^sub>2' \<longrightarrow> \<S> \<turnstile> m\<^sub>1 =\<^bsub>s,\<Gamma>\<^esub> m\<^sub>2 \<longrightarrow> \<S> \<turnstile> m\<^sub>1' =\<^bsub>s,\<Gamma>'\<^esub> m\<^sub>2'"
proof (cases \<alpha>)
  case (Store x i e)
  have "\<forall>v\<in>vars\<^sub>e (lift\<^sub>A i). m\<^sub>1 v = m\<^sub>2 v \<Longrightarrow> eval\<^sub>A m\<^sub>1 i = eval\<^sub>A m\<^sub>2 i" using eval\<^sub>A_det by blast
  then show ?thesis using Store 
    by (auto simp: supl_def  \<S>_def low_equiv1_def intro!: eval\<^sub>A_det)
next
  case (Load r x i)
  have "\<forall>v\<in>vars\<^sub>e (lift\<^sub>A i). m\<^sub>1 v = m\<^sub>2 v \<Longrightarrow> eval\<^sub>A m\<^sub>1 i = eval\<^sub>A m\<^sub>2 i" using eval\<^sub>A_det by blast
  then show ?thesis using Load by (auto simp: supl_def \<S>_def low_equiv1_def)
qed (auto simp: \<S>_def low_equiv1_def supl_def intro!: eval\<^sub>A_det)

lemma \<S>_sym_impl:
  "sym (\<S> i)"
  by (auto simp: sym_def \<S>_def)

end

sublocale type_rg_if_impl \<subseteq> typed_bisimulation update \<S> flow
  using \<S>_sym_impl flow_sound_impl update_det
  by (unfold_locales; blast)  

section \<open>Locale\<close>

context type_rg_if_impl
begin

abbreviation T
  where "T b \<equiv> orig (lift\<^sub>B b)"

abbreviation F
  where "F b \<equiv> orig (lift\<^sub>B (not b))"

abbreviation var_policy ("_:::_" [1000,100] 100)
  where "var_policy \<L> x \<equiv> lift\<^sub>C (\<L> x)"

abbreviation pol         
  where "pol \<L> \<equiv> forVars all_vars (\<lambda>y. (\<L> ::: y) \<sqsupseteq> (Type y))" 

subsection \<open>Executable Implementation\<close>

text \<open>
  Generate a predicate to demonstrate a low expression.
  Considers the global security policy as an initial optimisation,
  as some variables may be trivially low.
\<close>
definition type :: "('Var \<Rightarrow> 'Sexp) \<Rightarrow> 'Var \<Rightarrow> ('Var, nat, 'Sec) secexp"
  where "type \<L> y \<equiv> if (\<L> ::: y) = (Sec bot) then (Sec bot) else Type y \<sqinter> \<L> ::: y"

definition \<Gamma>\<^sub>e
  where "\<Gamma>\<^sub>e vs \<L> = forVars\<^sub>s vs (type \<L>)"

abbreviation \<Gamma>\<^sub>a where "\<Gamma>\<^sub>a e \<equiv> \<Gamma>\<^sub>e (vars\<^sub>A e)"
abbreviation \<Gamma>\<^sub>b where "\<Gamma>\<^sub>b e \<equiv> \<Gamma>\<^sub>e (vars\<^sub>B e)"

text \<open>
  The secureUpd proof obligation ensures that the assignment of e to x does not lower the 
  classification of any variables whilst they still hold classified information.
\<close>
definition ctrled
  where "ctrled x \<L> = filter (\<lambda>v. x \<in> set (vars\<^sub>C (\<L> v))) all_vars"

definition secureUpd 
  where "secureUpd x e \<L> \<equiv> forVars (ctrled x \<L>) (\<lambda>y. subst\<^sub>s (\<L> ::: y) x e \<sqsupseteq> type \<L> y)"

text \<open>
  Shorthand for the security proof obligation for an assignment x := e.
  v should be the variables of e. These are provided separately to facilitate optimisation.
\<close>
abbreviation PO\<^sub>a
  where "PO\<^sub>a s x e v \<L> \<equiv> \<L> ::: x \<sqsupseteq> \<Gamma>\<^sub>e v \<L> \<and>\<^sub>p secureUpd x e \<L> \<and>\<^sub>p (lift\<^sub>C s \<sqsupseteq> subst\<^sub>s (lift\<^sub>C s) x e)"  

text \<open>
  The PO proof obligation ensures that the assignment does not directly leak information and
  includes the secureUpd case.
\<close>
fun PO :: "'Sexp \<Rightarrow> ('Var,'Aexp,'Bexp) Action \<Rightarrow> ('Var\<Rightarrow>'Sexp) \<Rightarrow> ('Var,nat,'Sec) pred"
  where
    "PO s (x \<leftarrow> e) \<L> = PO\<^sub>a s x (lift\<^sub>A e) (vars\<^sub>A e) \<L>" | 
    "PO s (Guard b) \<L> = lift\<^sub>C s \<sqsupseteq> \<Gamma>\<^sub>b b \<L>" |
    "PO s (NCAS x e\<^sub>1 e\<^sub>2) \<L> = (lift\<^sub>C s \<sqsupseteq> type \<L> x \<and>\<^sub>p lift\<^sub>C s \<sqsupseteq> \<Gamma>\<^sub>a e\<^sub>1 \<L> \<and>\<^sub>p (PCmp (=) (Var x) (lift\<^sub>A e\<^sub>1) \<longrightarrow>\<^sub>p  PO\<^sub>a s x (lift\<^sub>A e\<^sub>2) (vars\<^sub>A e\<^sub>2) \<L>))" |
    "PO s (CAS x e\<^sub>1 e\<^sub>2) \<L> = (lift\<^sub>C s \<sqsupseteq> type \<L> x \<and>\<^sub>p lift\<^sub>C s \<sqsupseteq> \<Gamma>\<^sub>a e\<^sub>1 \<L> \<and>\<^sub>p (PCmp (=) (Var x) (lift\<^sub>A e\<^sub>1) \<longrightarrow>\<^sub>p PO\<^sub>a s x (lift\<^sub>A e\<^sub>2) (vars\<^sub>A e\<^sub>2) \<L>))" |
    "PO s (Store x i e) \<L> = (lift\<^sub>C s \<sqsupseteq> \<Gamma>\<^sub>a i \<L> \<and>\<^sub>p index_assert x (lift\<^sub>A i) (\<lambda>v. PO\<^sub>a s v (lift\<^sub>A e) (vars\<^sub>A e) \<L>))" |
    "PO s (Load r x i) \<L> = (lift\<^sub>C s \<sqsupseteq> \<Gamma>\<^sub>a i \<L> \<and>\<^sub>p index_assert x (lift\<^sub>A i) (\<lambda>v. PO\<^sub>a s r (Var v) [v] \<L>))" |
    "PO _ _ _ = Pb True"

text \<open>
  Shorthand for the guarantee proof obligation for an assignment x := e.
  v should be the variables of e. These are provided separately to facilitate optimisation.
\<close>
abbreviation guar\<^sub>a
  where "guar\<^sub>a x e v G \<L> \<equiv> unprime (subst\<^sub>\<Gamma>\<^sub>p (subst\<^sub>p G (Prime x) (orig\<^sub>e e)) (Prime x) (orig\<^sub>s (\<Gamma>\<^sub>e v \<L>)))"

text \<open>
  The guar proof obligation ensures that the operation conforms to some general guarantee condition G.
\<close>
fun guar :: "('Var,'Aexp,'Bexp) Action \<Rightarrow> ('Var,nat,'Sec) rpred \<Rightarrow> ('Var,'Sexp) Policy \<Rightarrow> ('Var,nat,'Sec) pred" 
  where 
    "guar (x \<leftarrow> e) G \<L> = guar\<^sub>a x (lift\<^sub>A e) (vars\<^sub>A e) G \<L>" |
    "guar (CAS x e\<^sub>1 e\<^sub>2) G \<L> = (PCmp (=) (Var x) (lift\<^sub>A e\<^sub>1) \<longrightarrow>\<^sub>p guar\<^sub>a x (lift\<^sub>A e\<^sub>2) (vars\<^sub>A e\<^sub>2) G \<L>)" |
    "guar (NCAS x e\<^sub>1 e\<^sub>2) G \<L> = (PCmp (=) (Var x) (lift\<^sub>A e\<^sub>1) \<longrightarrow>\<^sub>p guar\<^sub>a x (lift\<^sub>A e\<^sub>2) (vars\<^sub>A e\<^sub>2) G \<L>)" |
    "guar (Store x i e) G \<L> = index_assert x (lift\<^sub>A i) (\<lambda>v. guar\<^sub>a v (lift\<^sub>A e) (vars\<^sub>A e) G \<L>)" |
    "guar (Load r x i) G \<L> = index_assert x (lift\<^sub>A i) (\<lambda>v. guar\<^sub>a r (Var v) [v] G \<L>)" |
    "guar _ G \<L> = Pb True"

text \<open>
  Transform a context based on an action.
\<close>
fun wp\<^sub>Q :: "('Var,'Aexp,'Bexp) Action \<Rightarrow> ('Var,nat,'Sec) pred \<Rightarrow> ('Var,'Sexp) Policy \<Rightarrow> ('Var,nat,'Sec) pred" 
  where 
    "wp\<^sub>Q (x \<leftarrow> e) Q \<L> = wp\<^sub>a x (lift\<^sub>A e) (\<Gamma>\<^sub>a e \<L>) Q" | 
    "wp\<^sub>Q (Guard b) Q \<L> = wp\<^sub>b (lift\<^sub>B b) Q" |
    "wp\<^sub>Q (CAS x e\<^sub>1 e\<^sub>2) Q \<L> = wp\<^sub>b (PCmp (=) (Var x) (lift\<^sub>A e\<^sub>1)) (wp\<^sub>a x (lift\<^sub>A e\<^sub>2) (\<Gamma>\<^sub>a e\<^sub>2 \<L>) Q)" |
    "wp\<^sub>Q (NCAS x e _) Q \<L> = wp\<^sub>b (PCmp (\<noteq>) (Var x) (lift\<^sub>A e)) Q" |
    "wp\<^sub>Q (Store x i e) Q \<L> = wp\<^sub>l x (lift\<^sub>A i) (lift\<^sub>A e) (\<Gamma>\<^sub>a e \<L>) Q" |
    "wp\<^sub>Q (Load r x i) Q \<L> = wp\<^sub>a r (list\<^sub>e x (lift\<^sub>A i)) (list\<^sub>s x (lift\<^sub>A i) (type \<L>)) Q" |
    "wp\<^sub>Q _ Q \<L> = Q"

text \<open>
  Generate a stable predicate by priming the state and introducing a reflexive and transitive R
  to link the new current state with the original.
\<close>
definition stabilize :: "('Var, nat, 'Sec) rpred \<Rightarrow> ('Var, nat, 'Sec) pred \<Rightarrow> ('Var, nat, 'Sec) pred"
  where "stabilize R P \<equiv> unprime (pall (map Prime all_vars) (R \<longrightarrow>\<^sub>p (prime P)))"

definition assert :: "bool \<Rightarrow> (_,_,_) pred"
  where "assert b \<equiv> if b then Pb True else Pb False"

text \<open>The WP predicate transformer given an attacker level S and wellformed R, G and \<L>\<close>
fun wp :: "'Sexp \<Rightarrow> ('Var,nat,'Sec) rpred \<Rightarrow> (_,_,_) rpred \<Rightarrow> ('Var,'Sexp) Policy \<Rightarrow> (_,_,_,'Aexp,'Bexp) WPLang \<Rightarrow> (_,_,_) pred \<Rightarrow> (_,_,_) pred"
  where
    "wp S R G \<L> Skip Q = Q" |
    "wp S R G \<L> (Act \<alpha>) Q = stabilize R (guar \<alpha> G \<L> \<and>\<^sub>p PO S \<alpha> \<L> \<and>\<^sub>p wp\<^sub>Q \<alpha> Q \<L>)" |
    "wp S R G \<L> (Seq c\<^sub>1 c\<^sub>2) Q = wp S R G \<L> c\<^sub>1 (wp S R G \<L> c\<^sub>2 Q)" |
    "wp S R G \<L> (If \<alpha> c\<^sub>1 c\<^sub>2) Q = stabilize R (guar \<alpha> G \<L> \<and>\<^sub>p PO S \<alpha> \<L> \<and>\<^sub>p wp\<^sub>Q \<alpha> (wp S R G \<L> c\<^sub>1 Q) \<L> \<and>\<^sub>p wp\<^sub>Q (negate \<alpha>) (wp S R G \<L> c\<^sub>2 Q) \<L>)" |
    "wp S R G \<L> (While \<alpha> I c) Q = (stabilize R I \<and>\<^sub>p
                                   (assert (I \<turnstile>\<^sub>p (guar \<alpha> G \<L> \<and>\<^sub>p PO S \<alpha> \<L> \<and>\<^sub>p wp\<^sub>Q \<alpha> (wp S R G \<L> c (stabilize R I)) \<L>) \<and>\<^sub>p wp\<^sub>Q (negate \<alpha>) Q \<L>)))" |
    "wp S R G \<L> (DoWhile c I \<alpha>) Q = ((wp S R G \<L> c (stabilize R I)) \<and>\<^sub>p
                                   (assert (I \<turnstile>\<^sub>p (guar \<alpha> G \<L> \<and>\<^sub>p PO S \<alpha> \<L> \<and>\<^sub>p wp\<^sub>Q \<alpha> (wp S R G \<L> c (stabilize R I)) \<L>) \<and>\<^sub>p wp\<^sub>Q (negate \<alpha>) Q \<L>)))" |
    "wp S R G \<L> (TryUntil I c \<alpha>) Q = (stabilize R I \<and>\<^sub>p
                                   (assert (I \<turnstile>\<^sub>p wp S R G \<L> c (stabilize R (guar \<alpha> G \<L> \<and>\<^sub>p PO S \<alpha> \<L> \<and>\<^sub>p wp\<^sub>Q \<alpha> (stabilize R I) \<L> \<and>\<^sub>p wp\<^sub>Q (negate \<alpha>) Q \<L>)))))"

subsection \<open>State & Wellformedness Properties\<close>

text \<open>Convert a deeply-embedded state predicate into the shallow-embedding\<close>
definition state
  where "state P \<equiv> {(m,\<Gamma>). test m \<Gamma> P}"

text \<open>Convert a deeply-embedded state relation into the shallow-embedding\<close>
definition step
  where "step P \<equiv> {((m,\<Gamma>),(m',\<Gamma>')). test (m \<mapsto> m') (\<Gamma> \<mapsto> \<Gamma>') P}"

text \<open>Convert a deeply-embedded security policy into the shallow-embedding\<close>
definition secinv :: "('Var,'Sexp) Policy \<Rightarrow> (('Var,nat) Mem,'Var,'Sec) TPolicy"
  where "secinv \<L> \<equiv> \<lambda>m x. eval\<^sub>C m (\<L> x)"

text \<open>Constrain the security policy to prevent self-references\<close>
definition wf\<^sub>\<L> :: "('Var,'Sexp) Policy \<Rightarrow> bool"
  where "wf\<^sub>\<L> \<L> \<equiv> \<forall>x. x \<notin> set (vars\<^sub>C (\<L> x))"

text \<open>Couple all wellformedness conditions into a single definition\<close>
definition wellformed :: "('Var,nat,'Sec) rpred \<Rightarrow> ('Var,nat,'Sec) rpred \<Rightarrow> ('Var,'Sexp) Policy \<Rightarrow> ('Var,nat,'Sec) pred \<Rightarrow> bool"
  where "wellformed R G \<L> Q \<equiv> 
    stable (secinv \<L>) (state Q) (step R) \<and> reflexive R \<and> transitive R \<and> reflexive G \<and> wf\<^sub>\<L> \<L>"

lemma G_refl [intro]:
  assumes wf: "wellformed R G \<L> Q"
  shows "test (m \<mapsto> m) (\<Gamma> \<mapsto> \<Gamma>) G" 
  using wf by (auto simp: wellformed_def reflexive_def)

lemma wf\<^sub>\<L>_self [simp]:
  assumes "wellformed R G \<L> Q"
  shows "eval\<^sub>C (m(x:= e)) (\<L> x) = eval\<^sub>C m (\<L> x)"  
  using assms by (intro eval\<^sub>C_det) (auto simp: wf\<^sub>\<L>_def wellformed_def)

lemma secure_policy [simp]:
  "(m,\<Gamma>) \<in> (policy (secinv \<L>)) = (\<forall>x. eval\<^sub>C m (\<L> x) \<ge> \<Gamma> x)"
  unfolding policy_def secinv_def by simp

subsection \<open>Correctness of Implementation\<close>

lemma type [simp]:
  "eval\<^sub>s m \<Gamma> (type \<L> y) = eval\<^sub>s m \<Gamma> (Type y \<sqinter> \<L> ::: y)"
  unfolding type_def by (cases "\<L> ::: y"; auto)

lemma \<Gamma>_not [simp]:
  "\<Gamma>\<^sub>b (not b) = \<Gamma>\<^sub>b (b)"
  unfolding \<Gamma>\<^sub>e_def by auto

lemma secure_upd_nop [simp]:
  "v \<notin> set (ctrled x \<L>) \<Longrightarrow> eval\<^sub>C (m(x := eval m \<Gamma> e)) (\<L> v) = eval\<^sub>C m (\<L> v)"
  by (intro eval\<^sub>C_det) (simp add: ctrled_def)

lemma secure_upd [simp]:
  "test m \<Gamma> (secureUpd x e \<L>) = (\<forall>y. test m \<Gamma> (subst\<^sub>s (\<L> ::: y) x e \<sqsupseteq> (Type y \<sqinter> \<L> ::: y)))"
  by (auto simp add: secureUpd_def)

lemma \<Gamma>\<^sub>e_flow:
  assumes "(m, \<Gamma>) \<in> policy (secinv \<L>)"
  assumes "eval\<^sub>s m \<Gamma> (\<Gamma>\<^sub>e e \<L>) \<le> s"
  shows "\<forall>x\<in>set e. \<Gamma> x \<le> s"
  using assms by (simp add: supl_def \<Gamma>\<^sub>e_def) (simp add: inf.order_iff)

text \<open>
Given:
  1) Two memories m and m' are low equivalent.
  2) The security policy holds.
  3) 'b' is a low expression.
  Then: 
  The evaluation of the expression 'b' across the two memories m and m' will be the same.
\<close>
lemma low_exp:
  assumes "\<S> \<turnstile> m =\<^bsub>s,\<Gamma>\<^esub> m'" 
  assumes "\<forall>x. eval\<^sub>C m (\<L> x) \<ge> \<Gamma> x" 
  assumes "eval\<^sub>s m \<Gamma> (\<Gamma>\<^sub>a b \<L>) \<le> s m" 
  shows "eval\<^sub>A m b = eval\<^sub>A m' b" 
  using assms \<Gamma>\<^sub>e_flow eval\<^sub>A_det unfolding \<S>_def low_equiv1_def by auto

lemma low_guard:
  assumes "\<S> \<turnstile> m =\<^bsub>s,\<Gamma>\<^esub> m'" 
  assumes "\<forall>x. eval\<^sub>C m (\<L> x) \<ge> \<Gamma> x"
  assumes "eval\<^sub>s m \<Gamma> (\<Gamma>\<^sub>b b \<L>) \<le> s m"
  shows "eval\<^sub>B m b = eval\<^sub>B m' b"
  using assms \<Gamma>\<^sub>e_flow eval\<^sub>B_det unfolding \<S>_def low_equiv1_def by auto

lemma \<Gamma>\<^sub>e_correct [simp]:
  "(m,\<Gamma>) \<in> policy (secinv \<L>) \<Longrightarrow> eval\<^sub>s m \<Gamma> (\<Gamma>\<^sub>e e \<L>) = supl (map \<Gamma> e)"
  by (auto simp: \<Gamma>\<^sub>e_def inf_absorb1)

subsection \<open>Stability Properties\<close>

lemma prime_range [simp]: 
  "set (map Prime all_vars) = range Prime"
  by auto

lemma [simp]:
  "override_on m m' UNIV = m'"
  by auto

lemma univ_primedI:
  "(\<forall>m \<Gamma>. P (prime_state m) (prime_state \<Gamma>)) = (\<forall>m \<Gamma>. P m \<Gamma>)"
proof (intro iffI allI, goal_cases)
  case (1 m \<Gamma>)
  hence "P (prime_state (transition m m)) (prime_state (transition \<Gamma> \<Gamma>))" by blast
  then show ?case by simp
next
  case (2 m \<Gamma>)
  then show ?case by simp
qed

lemma stabilize:
  "test m \<Gamma> (stabilize R Q) = (\<forall>m' \<Gamma>'. test (m \<mapsto> m') (\<Gamma> \<mapsto> \<Gamma>') R \<longrightarrow> test m' \<Gamma>' Q)"
  unfolding stabilize_def test_unprime using test_pall_prime[OF prime_range]
  by (clarsimp) (rule univ_primedI)
                                            
text \<open>Show that a stabilized predicate is stable\<close>
lemma stabilize_stable [intro]:
  assumes wf: "wellformed R G \<L> P"
  shows "stable (secinv \<L>) (state (stabilize R Q)) (step R)"
  unfolding stable_def state_def step_def
proof (clarsimp)
  fix m \<Gamma> m' \<Gamma>'
  assume a: "test m \<Gamma> (stabilize R Q)" "test (m \<mapsto> m') (\<Gamma> \<mapsto> \<Gamma>') R"
  have "\<forall>m'' \<Gamma>''. test (m' \<mapsto> m'') (\<Gamma>' \<mapsto> \<Gamma>'') R \<longrightarrow> test (m \<mapsto> m'') (\<Gamma> \<mapsto> \<Gamma>'') R"
    using assms a(2) unfolding wellformed_def transitive_def by blast
  thus "test m' \<Gamma>' (stabilize R Q)" using a(1) by (auto simp: stabilize)
qed

text \<open>Stabilization preserves wellformedness\<close>
lemma stabilize_wellformed [intro]:
  assumes "wellformed R G \<L> P"
  shows "wellformed R G \<L> (stabilize R Q)" (is "wellformed R G \<L> ?P")
proof -
  have "stable (secinv \<L>) (state ?P) (step R)" 
    using stabilize_stable assms by blast
  thus ?thesis using assms unfolding wellformed_def by blast
qed

text \<open>Stabilization preserves entailment\<close>
lemma stabilize_entail [intro]:
  assumes "P \<turnstile>\<^sub>p Q"
  shows "stabilize R P \<turnstile>\<^sub>p stabilize R Q"
  using assms by (clarsimp simp add: entail_def stabilize)

text \<open>Stabilization preserves equivalence\<close>
lemma stabilize_equiv [intro]:
  assumes "P =\<^sub>p Q"
  shows "stabilize R P =\<^sub>p stabilize R Q"
  using assms stabilize_entail unfolding entail_def equiv_def by blast

text \<open>Elimination rule to ignore the stabilization process\<close>
lemma stabilize_asnE:
  assumes "wellformed R G \<L> Q"
  assumes "test m \<Gamma> (stabilize R P)"
  obtains "test m \<Gamma> P"
  using assms 
  by (auto simp: entail_def wellformed_def reflexive_def stabilize)

lemma [intro]:
  "stable (secinv \<L>)
     (state (Pb True))
     (step R)"
  by (auto simp: stable_def state_def)

text \<open>Allow the introduction of always\<close>
lemma wf_always [intro]:
  "wellformed R G \<L> Q \<Longrightarrow> wellformed R G \<L> (Q \<and>\<^sub>p assert P)"
  unfolding wellformed_def stable_def by (auto simp: state_def assert_def)

text \<open>Establish wellformedness of false\<close>
lemma wf_false [intro]:
  "\<forall>m \<Gamma>. \<not> test m \<Gamma> P \<Longrightarrow> wellformed R G \<L> Q \<Longrightarrow> wellformed R G \<L> P"
  unfolding wellformed_def stable_def by (auto simp: state_def)

text \<open>Establish wellformedness of true\<close>
lemma wf_true [intro]:
  "wellformed R G \<L> Q \<Longrightarrow> wellformed R G \<L> (Pb True)"
  unfolding wellformed_def stable_def by (auto simp: state_def)

subsection \<open>Act WP Properties\<close>

lemma negate_po:
  "(guar \<alpha> G \<L> \<and>\<^sub>p PO S \<alpha> \<L>) \<turnstile>\<^sub>p (guar (negate \<alpha>) G \<L> \<and>\<^sub>p PO S (negate \<alpha>) \<L>)"
  by (cases \<alpha>) auto

text \<open>WP enforces the progress property\<close>
lemma wp_progress [intro]:
  assumes "wellformed R G \<L> Q"
  shows "progress (\<lambda>m. eval\<^sub>C m S) (secinv \<L>) (state (wp S R G \<L> (WPLang.Act \<alpha>) Q)) \<alpha>"
  using low_guard low_exp
proof (cases \<alpha>)
  case (NCAS x e\<^sub>1 e\<^sub>2)
  then show ?thesis using low_guard low_exp
    apply (auto simp: progress_def low_equiv_def state_def elim!: stabilize_asnE[OF assms])
    by (simp add: low_equiv1_def \<S>_def inf.order_iff)
next
  case (CAS x e\<^sub>1 e\<^sub>2)
  then show ?thesis using low_guard low_exp
    apply (auto simp: progress_def low_equiv_def state_def elim!: stabilize_asnE[OF assms])
    by (simp_all add: low_equiv1_def \<S>_def inf.order_iff)
qed (auto simp: progress_def low_equiv_def state_def elim!: stabilize_asnE[OF assms])

text \<open>WP correctly transforms the state\<close>
lemma wp_functional [intro]:
  assumes wf: "wellformed R G \<L> Q"
  shows "\<lfloor>policy (secinv \<L>) \<inter> state (wp S R G \<L> (Act \<alpha>) Q)\<rfloor> \<inter> eval\<^sub>t (\<lambda>m. eval\<^sub>C m S) \<alpha> \<subseteq> \<lceil>state Q\<rceil>"
  by (cases \<alpha>) (auto simp: pre_def post_def eval\<^sub>t_def state_def elim!: stabilize_asnE[OF wf]; metis inf.orderE)+

text \<open>WP enforces the guarantee\<close>
lemma wp_guarantee [intro]:
  assumes wf: "wellformed R G \<L> Q"
  shows "\<lfloor>policy (secinv \<L>) \<inter> state (wp S R G \<L> (Act \<alpha>) Q)\<rfloor> \<inter> eval\<^sub>t (\<lambda>m. eval\<^sub>C m S) \<alpha> \<subseteq> step G"
  using assms by (cases \<alpha>) (auto simp: step_def pre_def eval\<^sub>t_def state_def supl_def elim!: stabilize_asnE[OF wf])

text \<open>WP preserves the security policy\<close>
lemma wp_secure [intro]:
  assumes wf: "wellformed R G \<L> Q"
  shows "\<lfloor>policy (secinv \<L>) \<inter> state (wp S R G \<L> (Act \<alpha>) Q)\<rfloor> \<inter> eval\<^sub>t (\<lambda>m. eval\<^sub>C m S) \<alpha> \<subseteq> \<lceil>policy (secinv \<L>)\<rceil>"
  using assms by (cases \<alpha>) (auto simp: step_def pre_def post_def eval\<^sub>t_def state_def \<Gamma>\<^sub>e_def inf_absorb1 supl_def elim!: stabilize_asnE[OF wf])

text \<open>WP enforces falling attacker\<close>
lemma wp_falling [intro]:
  assumes wf: "wellformed R G \<L> Q"
  shows "falling (\<lambda>m. eval\<^sub>C m S) (secinv \<L>) (state (wp S R G \<L> (WPLang.Act \<alpha>) Q)) \<alpha>"
  by (cases \<alpha>) (auto simp: step_def pre_def eval\<^sub>t_def state_def supl_def falling_def elim!: stabilize_asnE[OF wf])

text \<open>WP preserves wellformedness\<close>
lemma wellformed_actI [intro]:
  assumes "wellformed R G \<L> Q"
  shows "wellformed R G \<L> (wp S R G \<L> (WPLang.Act \<alpha>) Q)" (is "wellformed R G \<L> ?P")
  using assms by simp (meson stabilize_wellformed) 

lemma wp_defined [intro]:
  assumes wf: "wellformed R G \<L> Q"
  shows "defined (\<lambda>m. eval\<^sub>C m S) (secinv \<L>) (state (wp S R G \<L> (WPLang.Act \<alpha>) Q)) \<alpha>"
  by (cases \<alpha>) (auto simp: defined_def state_def elim!: stabilize_asnE[OF wf])

text \<open>WP corresponds to the RG rule for actions\<close>
lemma actI [intro]:
  assumes "wellformed R G \<L> Q"
  shows "rules (step (R)) (step G) (\<lambda>m. eval\<^sub>C m S) (secinv \<L>) (state (wp S R G \<L> (Act \<alpha>) Q)) (Basic \<alpha>) (state Q)"
    (is "rules ?R ?G ?s ?\<L> ?P (Basic \<alpha>) ?Q")
proof (rule rules.act)
  show "act_po ?G ?s ?\<L> ?P \<alpha> ?Q"
    using assms wp_functional wp_guarantee wp_secure wp_falling wp_defined by (auto simp: act_po_def)
qed (insert assms, auto simp: wellformed_def)

text \<open>Couple the above with a rewrite to generalize the state\<close>
lemma actI_rw [intro]:
  assumes "wellformed R G \<L> Q"
  assumes "P \<turnstile>\<^sub>p wp S R G \<L> (WPLang.Act \<alpha>) Q"
  shows "rules (step R) (step G) (\<lambda>m. eval\<^sub>C m S) (secinv \<L>) (state P) (Basic \<alpha>) (state Q)"
  using actI[OF assms(1)]
proof (rule rules.rewrite)
  show "state P \<subseteq> state (wp S R G \<L> (WPLang.Act \<alpha>) Q)"
    using assms(2) by (auto simp: entail_def state_def)
qed auto

subsection \<open>Abstract Definitions\<close>

text \<open>Convert the While language into the general version\<close>
fun lift\<^sub>c :: "('Var, nat, 'Sec, 'Aexp, 'Bexp) WPLang \<Rightarrow> ('Var, 'Aexp, 'Bexp) Action Stmt"
  where
    "lift\<^sub>c Skip = Stmt.Skip" |
    "lift\<^sub>c (Act \<alpha>) = (Basic \<alpha>)"|
    "lift\<^sub>c (Seq c\<^sub>1 c\<^sub>2) = (Stmt.Seq (lift\<^sub>c c\<^sub>1) (lift\<^sub>c c\<^sub>2))" |
    "lift\<^sub>c (If \<alpha> c\<^sub>1 c\<^sub>2) = (Choice (Stmt.Seq (Basic \<alpha>) (lift\<^sub>c c\<^sub>1)) (Stmt.Seq (Basic (negate \<alpha>)) (lift\<^sub>c c\<^sub>2)))" |
    "lift\<^sub>c (While \<alpha> I c) = (Stmt.Seq ((Stmt.Seq (Basic \<alpha>) (lift\<^sub>c c))*) (Basic (negate \<alpha>)))" |
    "lift\<^sub>c (DoWhile c I \<alpha>) = (Stmt.Seq (lift\<^sub>c c) (Stmt.Seq ((Stmt.Seq (Basic \<alpha>) (lift\<^sub>c c))*) (Basic (negate \<alpha>))))" |
    "lift\<^sub>c (TryUntil I c \<alpha>) = (Stmt.Seq ((Stmt.Seq (lift\<^sub>c c) (Basic \<alpha>))*) (Stmt.Seq (lift\<^sub>c c) (Basic (negate \<alpha>))))"

text \<open>An ordering property on contexts\<close>
definition context_order :: "('Var,nat,'Sec) rpred \<Rightarrow> ('Var,nat,'Sec) rpred \<Rightarrow> ('Var,'Sexp) Policy \<Rightarrow> ('Var,nat,'Sec) pred \<Rightarrow> ('Var, nat, 'Sec) pred \<Rightarrow> bool" 
  ("_,_,_ \<turnstile> _ \<ge> _" [70,0,0,0,70] 100)
  where "context_order R G \<L> P Q \<equiv> wellformed R G \<L> Q \<longrightarrow> ((P \<turnstile>\<^sub>p Q) \<and> wellformed R G \<L> P)"

text \<open>The validity property we intend to show, abstracts over the preservation of wellformedness\<close>
definition valid :: "'Sexp \<Rightarrow> ('Var,nat,'Sec) rpred \<Rightarrow> (_,_,_) rpred \<Rightarrow> (_,_) Policy \<Rightarrow> (_,_,_) pred \<Rightarrow> (_,_,_,'Aexp,'Bexp) WPLang \<Rightarrow> (_,_,_) pred \<Rightarrow> bool" 
  ("_,_,_,_ \<turnstile> _ {_} _" [100,0,0,0,0,0,0] 60)
  where "valid S R G \<L> P c Q \<equiv> wellformed R G \<L> Q \<longrightarrow> 
    (rules (step (R)) (step G) (\<lambda>m. eval\<^sub>C m S) (secinv \<L>) (state P) (lift\<^sub>c c) (state Q) \<and> wellformed R G \<L> P)"

definition if_secure :: "'Sexp \<Rightarrow> ('Var,nat,'Sec) rpred \<Rightarrow> ('Var,nat,'Sec) rpred \<Rightarrow> ('Var,'Sexp) Policy \<Rightarrow> ('Var,nat,'Sec) pred \<Rightarrow> ('Var, nat, 'Sec, 'Aexp, 'Bexp) WPLang \<Rightarrow> bool"  ("0_,_,_ \<turnstile> R: _ /G: _ /{_}" [0, 0, 0, 0, 0] 61)
  where "if_secure S R G \<L> P c \<equiv> secure (\<lambda>m. eval\<^sub>C m S) (secinv \<L>) (state P) (lift\<^sub>c c)"

method act_tac = intro actI_rw; simp; intro stabilize_entail; insert negate_po; auto simp: entail_def

subsection \<open>Base Introduction Rules\<close>

text \<open>Reflexive Ordering\<close>
lemma context_order_refl [intro]:
  "R,G,\<L> \<turnstile> P \<ge> P"
  unfolding context_order_def by auto

text \<open>Always Ordering\<close>
lemma alwaysOrd:
  "R,G,\<L> \<turnstile> P \<and>\<^sub>p assert A \<ge> P"
  by (cases A) (auto simp: context_order_def) 

text \<open>Stabilize Ordering\<close>
lemma stabilizeOrd:
  assumes "P \<turnstile>\<^sub>p Q" 
  shows " R,G,\<L> \<turnstile> stabilize R P \<ge> Q"
  using assms unfolding context_order_def entail_def
  by (meson stabilize_asnE stabilize_wellformed subrelI subset_iff)

text \<open>Skip Rule\<close>
lemma skipWP:
  "S,R,G,\<L> \<turnstile> Q {Skip} Q"
  by (auto simp: context_order_def valid_def wellformed_def)

text \<open>Act Rule\<close>
lemma actWP:
  "S,R,G,\<L> \<turnstile> wp S R G \<L> (Act \<alpha>) Q {Act \<alpha>} Q"
  unfolding valid_def lift\<^sub>c.simps by blast

text \<open>Sequential Rule\<close>
lemma seqWP:
  "S,R,G,\<L> \<turnstile> P {c\<^sub>1} Q \<Longrightarrow> S,R,G,\<L> \<turnstile> M {c\<^sub>2} P \<Longrightarrow> S,R,G,\<L> \<turnstile> M {c\<^sub>2 ; c\<^sub>1} Q"
  unfolding valid_def by auto

text \<open>If Rule\<close>
lemma ifWP:
  assumes "S,R,G,\<L> \<turnstile> P\<^sub>1 {c\<^sub>1} Q"
  assumes "S,R,G,\<L> \<turnstile> P\<^sub>2 {c\<^sub>2} Q"
  shows "S,R,G,\<L> \<turnstile> stabilize R (guar \<alpha> G \<L> \<and>\<^sub>p PO S \<alpha> \<L> \<and>\<^sub>p wp\<^sub>Q \<alpha> P\<^sub>1 \<L>  \<and>\<^sub>p wp\<^sub>Q (negate \<alpha>) P\<^sub>2 \<L>) {If \<alpha> c\<^sub>1 c\<^sub>2} Q"
  using assms apply (clarsimp simp add: valid_def)
  apply (intro impI conjI rules.choice rules.seq stabilize_wellformed; assumption?)
  by act_tac+

text \<open>While Rule\<close>
lemma whileWP:
  assumes "I \<turnstile>\<^sub>p guar \<alpha> G \<L> \<and>\<^sub>p PO S \<alpha> \<L> \<and>\<^sub>p wp\<^sub>Q \<alpha> P \<L> \<and>\<^sub>p wp\<^sub>Q (negate \<alpha>) Q \<L>"
  assumes "S,R,G,\<L> \<turnstile> P {c} stabilize R I" (is "S,R,G,\<L> \<turnstile> P {c} ?I")
  shows "S,R,G,\<L> \<turnstile> ?I {While \<alpha> I c} Q"
  unfolding valid_def lift\<^sub>c.simps
proof (intro impI conjI rules.choice rules.seq stabilize_wellformed; assumption?)
  assume "wellformed R G \<L> Q"
  thus "rules (step R) (step G)
     (\<lambda>m. eval\<^sub>C m S) (secinv \<L>) (state ?I)
     (Basic (negate \<alpha>)) (state Q)" using assms(1) by (act_tac; fast)
next
  assume wf: "wellformed R G \<L> Q"
  hence "wellformed R G \<L> ?I" by auto
  thus "rules (step R) (step G) (\<lambda>m. eval\<^sub>C m S) (secinv \<L>) (state ?I) ((Basic \<alpha> ;; lift\<^sub>c c)*) (state ?I)"
    using assms
    apply (intro rules.loop rules.seq stabilize_stable; simp add: valid_def)
    defer 1
    by blast act_tac
qed

text \<open>Do While Rule\<close>
lemma doWhileWP:
  assumes "I \<turnstile>\<^sub>p guar \<alpha> G \<L> \<and>\<^sub>p PO S \<alpha> \<L> \<and>\<^sub>p wp\<^sub>Q \<alpha> P \<L> \<and>\<^sub>p wp\<^sub>Q (negate \<alpha>) Q \<L>"
  assumes "S,R,G,\<L> \<turnstile> P {c} (stabilize R I)" (is "S,R,G,\<L> \<turnstile> P {c} ?I")
  shows "S,R,G,\<L> \<turnstile> P {DoWhile c I \<alpha>} Q"
  unfolding valid_def lift\<^sub>c.simps
proof (intro impI conjI rules.choice rules.seq stabilize_wellformed)
  assume "wellformed R G \<L> Q"
  thus "rules (step R) (step G)
     (\<lambda>m. eval\<^sub>C m S) (secinv \<L>) (state ?I)
     (Basic (negate \<alpha>)) (state Q)" using assms(1) by (act_tac; fast)
next
  assume wf: "wellformed R G \<L> Q"
  hence "wellformed R G \<L> ?I" by auto
  thus "rules (step R) (step G) (\<lambda>m. eval\<^sub>C m S) (secinv \<L>) (state ?I) ((Basic \<alpha> ;; lift\<^sub>c c)*) (state ?I)"
    using assms
    apply (intro rules.loop rules.seq stabilize_stable; simp add: valid_def)
    defer 1
    by blast act_tac
qed (insert assms(2), auto simp: valid_def)

text \<open>Try Until Rule\<close>
lemma tryUntilWP:
  assumes "S,R,G,\<L> \<turnstile> stabilize R I {c} (stabilize R (guar \<alpha> G \<L> \<and>\<^sub>p PO S \<alpha> \<L> \<and>\<^sub>p wp\<^sub>Q \<alpha> (stabilize R I) \<L> \<and>\<^sub>p wp\<^sub>Q (negate \<alpha>) Q \<L>))" (is "S,R,G,\<L> \<turnstile> ?I {c} ?Q")
  shows "S,R,G,\<L> \<turnstile> stabilize R I {TryUntil I c \<alpha>} Q"
  unfolding valid_def lift\<^sub>c.simps
proof (intro impI conjI rules.choice rules.seq stabilize_wellformed; assumption?)
  assume "wellformed R G \<L> Q"
  thus "rules (step R) (step G)
     (\<lambda>m. eval\<^sub>C m S) (secinv \<L>) (state ?Q)
     (Basic (negate \<alpha>)) (state Q)" by act_tac 
next
  show "wellformed R G \<L> Q \<Longrightarrow> rules (step R) (step G) (\<lambda>m. eval\<^sub>C m S) (secinv \<L>) (state ?I) (lift\<^sub>c c) (state ?Q)" using assms(1) by (auto simp: valid_def)
next
  assume wf: "wellformed R G \<L> Q"
  hence "wellformed R G \<L> ?I" by auto
  thus "rules (step R) (step G) (\<lambda>m. eval\<^sub>C m S) (secinv \<L>) (state ?I) ((lift\<^sub>c c ;; Basic \<alpha>)*) (state ?I)"
    using assms
    apply (intro rules.loop rules.seq stabilize_stable; simp add: valid_def)
    by blast act_tac
qed

text \<open>False Rule\<close>
lemma falseWP:
  assumes "P \<turnstile>\<^sub>p Pb False"
  shows "S,R,G,\<L> \<turnstile> P {c} Q"
  using assms unfolding valid_def
  by (intro conjI impI rewrite[OF falseI]) (auto simp: entail_def state_def)

text \<open>Precondition Rewrite Rule\<close>
lemma rewriteWP:
  "S,R,G,\<L> \<turnstile> P {c} Q \<Longrightarrow> R,G,\<L> \<turnstile> M \<ge> P \<Longrightarrow> S,R,G,\<L> \<turnstile> M {c} Q"
proof (clarsimp simp add: valid_def context_order_def)
  assume a: "M \<turnstile>\<^sub>p P"
  assume "rules (step R) (step G) (\<lambda>m. eval\<^sub>C m S) (secinv \<L>) (state P) (lift\<^sub>c c) (state Q)"
  thus "rules (step R) (step G)
     (\<lambda>m. eval\<^sub>C m S) (secinv \<L>)
     (state M) (lift\<^sub>c c) (state Q)" by (rule rules.rewrite) (insert a, auto simp: entail_def state_def)
qed

text \<open>Assert Rule\<close>
lemma alwaysWP:
  assumes "A \<longrightarrow> S,R,G,\<L> \<turnstile> P {c} Q"
  shows "S,R,G,\<L> \<turnstile> (P \<and>\<^sub>p assert A) {c} Q"
  using assms 
  apply (cases A)
  apply (intro rewriteWP[OF _ alwaysOrd], auto)
  by (intro falseWP, auto simp: entail_def assert_def)

subsection \<open>Rewrite Introduction Rules\<close>

text \<open>Skip Rewrite Rule\<close>
lemma skipRW:
  assumes "R,G,\<L> \<turnstile> P \<ge> Q" 
  shows "S,R,G,\<L> \<turnstile> P {Skip} Q"
  using rewriteWP[OF skipWP assms] .

text \<open>Act Rewrite Rule\<close>
lemma actRW:
  assumes "R,G,\<L> \<turnstile> P \<ge> wp S R G \<L> (Act \<alpha>) Q"
  shows "S,R,G,\<L> \<turnstile> P {Act \<alpha>} Q"
  using rewriteWP[OF actWP assms] .

text \<open>If Rewrite Rule\<close>
lemma ifRW:
  assumes "R,G,\<L> \<turnstile> P \<ge> stabilize R (guar \<alpha> G \<L> \<and>\<^sub>p PO S \<alpha> \<L> \<and>\<^sub>p wp\<^sub>Q \<alpha> P\<^sub>1 \<L> \<and>\<^sub>p wp\<^sub>Q (negate \<alpha>) P\<^sub>2 \<L>)"
  assumes "S,R,G,\<L> \<turnstile> P\<^sub>1 {c\<^sub>1} Q"
  assumes "S,R,G,\<L> \<turnstile> P\<^sub>2 {c\<^sub>2} Q"
  shows "S,R,G,\<L> \<turnstile> P {If \<alpha> c\<^sub>1 c\<^sub>2} Q"
  using rewriteWP[OF ifWP[OF assms(2,3)] assms(1)] . 

text \<open>While Rewrite Rule\<close>
lemma whileRW:
  assumes order: "R,G,\<L> \<turnstile> N \<ge> stabilize R I"
  assumes recur: "S,R,G,\<L> \<turnstile> P {c} stabilize R I"
  assumes side: "I \<turnstile>\<^sub>p guar \<alpha> G \<L> \<and>\<^sub>p PO S \<alpha> \<L> \<and>\<^sub>p wp\<^sub>Q \<alpha> P \<L> \<and>\<^sub>p wp\<^sub>Q (negate \<alpha>) Q \<L>"
  shows "S,R,G,\<L> \<turnstile> N {While \<alpha> I c} Q"
  using rewriteWP[OF whileWP[OF side recur] order] .

text \<open>Do While Rewrite Rule, same as the normal\<close>
lemma doWhileRW:
  assumes "I \<turnstile>\<^sub>p guar \<alpha> G \<L> \<and>\<^sub>p PO S \<alpha> \<L> \<and>\<^sub>p wp\<^sub>Q \<alpha> P \<L> \<and>\<^sub>p wp\<^sub>Q (negate \<alpha>) Q \<L>"
  assumes "S,R,G,\<L> \<turnstile> P {c} (stabilize R I)" (is "S,R,G,\<L> \<turnstile> P {c} ?I")
  shows "S,R,G,\<L> \<turnstile> P {DoWhile c I \<alpha>} Q"
  using assms doWhileWP by auto

text \<open>Try Until Rewrite Rule\<close>
lemma tryUntilRW:
  assumes order: "R,G,\<L> \<turnstile> P \<ge> stabilize R I"
  assumes recur: "S,R,G,\<L> \<turnstile> stabilize R I {c} (stabilize R (guar \<alpha> G \<L> \<and>\<^sub>p PO S \<alpha> \<L> \<and>\<^sub>p wp\<^sub>Q \<alpha> (stabilize R I) \<L> \<and>\<^sub>p wp\<^sub>Q (negate \<alpha>) Q \<L>))" (is "S,R,G,\<L> \<turnstile> ?I {c} ?Q")
  shows "S,R,G,\<L> \<turnstile> P {TryUntil I c \<alpha>} Q"
  using rewriteWP[OF tryUntilWP[OF recur] order] .

subsection \<open>Soundness\<close>

lemma wp_valid:
  shows "S,R,G,\<L> \<turnstile> wp S R G \<L> c Q {c} Q"
proof (induct c arbitrary: Q)
  case Skip
  thus ?case using skipWP by auto
next
  case (Act x)
  thus ?case using actWP by auto
next
  case (Seq c1 c2)
  thus ?case using seqWP by fastforce
next
  case (If x1 c1 c2)
  thus ?case using ifWP by auto
next
  case (While \<alpha> I c)
  thus ?case unfolding wp.simps
    by (intro alwaysWP impI whileRW) (auto simp: entail_def)
next
  case (DoWhile c I b)
  thus ?case unfolding wp.simps
    by (intro alwaysWP impI doWhileRW) (auto simp: entail_def)
next
  case (TryUntil I c b)
  thus ?case unfolding wp.simps
    by (intro alwaysWP impI tryUntilWP; rule rewriteWP) (blast intro: stabilizeOrd)
qed

theorem if_secureI:
  assumes wf: "transitive R" "reflexive R" "reflexive G" "wf\<^sub>\<L> \<L>"
  assumes v: "S,R,G,\<L> \<turnstile> P {c} Pb True"
  assumes e: "P\<^sub>0 \<turnstile>\<^sub>p P"
  shows "if_secure S R G \<L> P\<^sub>0 c"
  unfolding if_secure_def
proof (rule secure_bisim, rule rules.rewrite)
  have "wellformed R G \<L> (Pb True)" using wf by (auto simp add: wellformed_def)
  thus "rules (step R) (step G) (\<lambda>m. eval\<^sub>C m S) (secinv \<L>) (state P) (lift\<^sub>c c) (state (Pb True))" 
    using v e by (auto simp: valid_def)
qed (insert e; auto simp: state_def entail_def)+

theorem wp_sound:
  assumes wf: "transitive R" "reflexive R" "reflexive G" "wf\<^sub>\<L> \<L>"
  assumes e: "P \<turnstile>\<^sub>p wp S R G \<L> c (Pb True)"
  shows "if_secure S R G \<L> P c"
  by (rule if_secureI[OF wf _ e]) (rule wp_valid)

end

end