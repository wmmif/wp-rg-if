theory Typed_Bisimulation
  imports Security
begin

text \<open>
Prove soundness for a RG-based Hoare logic capable of demonstrating low equivalence.
\<close>

locale typed_bisimulation = security eval \<S>
  for eval :: "'a \<Rightarrow> 'm rel"
  and \<S> :: "'i \<Rightarrow> 'm rel" +
  fixes flow :: "('m,'s::bounded_lattice) TType \<Rightarrow> 'a \<Rightarrow> 'm \<Rightarrow> ('i,'s) Ctx rel"
  assumes flow_sound: "(m\<^sub>1,m\<^sub>1') \<in> eval \<alpha> \<Longrightarrow> (m\<^sub>2,m\<^sub>2') \<in> eval \<alpha> \<Longrightarrow> (\<Gamma>,\<Gamma>') \<in> flow s \<alpha> m\<^sub>1 \<Longrightarrow> s m\<^sub>1 \<ge> s m\<^sub>1' \<Longrightarrow> s m\<^sub>2 \<ge> s m\<^sub>2' \<Longrightarrow>  \<S> \<turnstile> m\<^sub>1 =\<^bsub>s,\<Gamma>\<^esub> m\<^sub>2 \<Longrightarrow> \<S> \<turnstile> m\<^sub>1' =\<^bsub>s,\<Gamma>'\<^esub> m\<^sub>2'"

context typed_bisimulation
begin

section \<open>Rules\<close>

text \<open>Stability of a state across an environment step, given an invariant security policy\<close>
definition stable :: "('m,'i,'s) TPolicy \<Rightarrow> ('m,'i,'s) TState \<Rightarrow> (_,_,_) TStep \<Rightarrow> bool" 
  where "stable \<L> \<equiv> \<lambda>f g. (\<forall>x y. x \<in> f \<longrightarrow> (x, y) \<in> (invPolicy \<L> \<inter> g)  \<longrightarrow> y \<in> f)"

text \<open>Progress of an action under one state implies progress under the bisimilar state\<close>
definition progress :: "('m,'s::bounded_lattice) TType \<Rightarrow> ('m,'i,'s) TPolicy \<Rightarrow> ('m,'i,'s) TState \<Rightarrow> 'a \<Rightarrow> bool"
  where "progress s \<L> P \<alpha> \<equiv> \<forall>m\<^sub>1 m\<^sub>2. m\<^sub>1 =\<^bsub>s,\<L>,P\<^esub> m\<^sub>2 \<longrightarrow> (\<exists>m\<^sub>1'. (m\<^sub>1, m\<^sub>1') \<in> eval \<alpha>) \<longrightarrow> (\<exists>m\<^sub>2'. (m\<^sub>2, m\<^sub>2') \<in> eval \<alpha>)"

text \<open>Lift an action into a step\<close>
definition eval\<^sub>t :: "('m,'s::bounded_lattice) TType \<Rightarrow> 'a \<Rightarrow> ('m,'i,'s) TStep" 
  where "eval\<^sub>t s \<alpha> \<equiv> {((m,\<Gamma>),(m',\<Gamma>')). (m,m') \<in> eval \<alpha> \<and> (\<Gamma>,\<Gamma>') \<in> flow s \<alpha> m}"

text \<open>Constrain the pre of a step\<close>
definition pre :: "('Var,'Val,'Sec) TState \<Rightarrow> (_,_,_) TStep" ("\<lfloor>_\<rfloor>" 80)
  where "pre P \<equiv> {((m\<^sub>1,m\<^sub>2),(m\<^sub>1',m\<^sub>2')). (m\<^sub>1,m\<^sub>2) \<in> P}"

text \<open>Constrain the post-state of a step\<close>
definition post :: "('Var,'Val,'Sec) TState \<Rightarrow> (_,_,_) TStep" ("\<lceil>_\<rceil>" 80)
  where "post P \<equiv> {((m\<^sub>1,m\<^sub>2),(m\<^sub>1',m\<^sub>2')). (m\<^sub>1',m\<^sub>2') \<in> P}"

text \<open>Constrain the attacker level to only fall (temporary)\<close>
definition falling
  where "falling s \<L> P \<alpha> \<equiv> \<forall>m \<Gamma> m'. (m,\<Gamma>) \<in> policy \<L> \<inter> P \<longrightarrow> (m,m') \<in> eval \<alpha> \<longrightarrow> s m' \<le> s m"

definition defined
  where "defined s \<L> P \<alpha> \<equiv> \<forall>m \<Gamma> m'. (m,\<Gamma>) \<in> policy \<L> \<inter> P \<longrightarrow> (m,m') \<in> eval \<alpha> \<longrightarrow> (\<exists>\<Gamma>'. (\<Gamma>,\<Gamma>') \<in> flow s \<alpha> m)"

text \<open>Couple all of the proof obligations related to an action\<close>
definition act_po
  where "act_po G s \<L> P \<alpha> Q \<equiv> 
    defined s \<L> P \<alpha> \<and>
    falling s \<L> P \<alpha> \<and> 
    progress s \<L> P \<alpha> \<and> 
    \<lfloor>policy \<L> \<inter> P\<rfloor> \<inter> eval\<^sub>t s \<alpha> \<subseteq> G \<inter> \<lceil>policy \<L> \<inter> Q\<rceil>"

inductive rules :: 
  "('m,'i,'s) TStep \<Rightarrow> (_,_,_) TStep \<Rightarrow> ('m,'s::bounded_lattice) TType \<Rightarrow> (_,_,_) TPolicy \<Rightarrow> (_,_,_) TState \<Rightarrow> 'a Stmt \<Rightarrow> (_,_,_) TState \<Rightarrow> bool"
  ("_,_,_ \<turnstile>\<^sub>_ _ {_} _" [20, 20, 20, 20,20] 20)
where
  skip [intro]:     "stable \<L> P R \<Longrightarrow> R,G,s \<turnstile>\<^sub>\<L> P { Skip } P" |
  seq [intro]:      "R,G,s \<turnstile>\<^sub>\<L> P { c\<^sub>1 } Q \<Longrightarrow> R,G,s \<turnstile>\<^sub>\<L> Q { c\<^sub>2 } M \<Longrightarrow> R,G,s \<turnstile>\<^sub>\<L> P { c\<^sub>1 ;; c\<^sub>2 } M" |
  choice [intro]:   "R,G,s \<turnstile>\<^sub>\<L> P { c\<^sub>1 } Q \<Longrightarrow> R,G,s \<turnstile>\<^sub>\<L> P { c\<^sub>2 } Q \<Longrightarrow> R,G,s \<turnstile>\<^sub>\<L> P { c\<^sub>1 \<Sqinter> c\<^sub>2 } Q" |
  loop [intro]:     "stable \<L> P R \<Longrightarrow> R,G,s \<turnstile>\<^sub>\<L> P { c } P \<Longrightarrow> R,G,s \<turnstile>\<^sub>\<L> P { c* } P" |
  act [intro]:      "stable \<L> P R \<Longrightarrow> stable \<L> Q R \<Longrightarrow> act_po G s \<L> P \<alpha> Q \<Longrightarrow> R,G,s \<turnstile>\<^sub>\<L> P { Basic \<alpha> } Q" |
  rewrite [intro]:  "R,G,s \<turnstile>\<^sub>\<L> P { c } Q \<Longrightarrow> P' \<subseteq> P \<Longrightarrow> R' \<subseteq> R \<Longrightarrow> G \<subseteq> G' \<Longrightarrow> Q \<subseteq> Q' \<Longrightarrow> 
                      R',G',s \<turnstile>\<^sub>\<L> P' { c } Q'" |
  parallel [intro]: "R\<^sub>1,G\<^sub>1,s \<turnstile>\<^sub>\<L> P\<^sub>1 { c\<^sub>1 } Q\<^sub>1 \<Longrightarrow> R\<^sub>2,G\<^sub>2,s \<turnstile>\<^sub>\<L> P\<^sub>2 { c\<^sub>2 } Q\<^sub>2 \<Longrightarrow> G\<^sub>2 \<subseteq> R\<^sub>1 \<Longrightarrow> G\<^sub>1 \<subseteq> R\<^sub>2 \<Longrightarrow>
                      R\<^sub>1 \<inter> R\<^sub>2,G\<^sub>1 \<union> G\<^sub>2,s \<turnstile>\<^sub>\<L> P\<^sub>1 \<inter> P\<^sub>2 { c\<^sub>1 || c\<^sub>2 } Q\<^sub>1 \<inter> Q\<^sub>2"

lemma stableI [intro]:
  assumes "stable \<L> P R" "R' \<subseteq> R"
  shows "stable \<L> P R'"
  using assms by (auto simp: stable_def)

lemma stable_conj [intro]:
  assumes "stable \<L> P R" "stable \<L> P' R'"
  shows "stable \<L> (P \<inter> P') (R \<inter> R')"
  using assms by (auto simp: stable_def)

lemma bisim_act_imp [intro]:
  assumes "progress s \<L> P \<alpha>" "Q \<subseteq> P"
  shows "progress s \<L> Q \<alpha>"
  using assms low_equiv_subI 
  unfolding progress_def by blast

lemma pre_subI [intro]:
  "P \<subseteq> P' \<Longrightarrow> \<lfloor>P\<rfloor> \<subseteq> \<lfloor>P'\<rfloor>"
  unfolding pre_def by auto

lemma post_subI [intro]:
  "P \<subseteq> P' \<Longrightarrow> \<lceil>P\<rceil> \<subseteq> \<lceil>P'\<rceil>"
  unfolding post_def by auto

lemma post_conj [simp]:
  "\<lceil>Q \<inter> M\<rceil> = \<lceil>Q\<rceil> \<inter> \<lceil>M\<rceil>"
  by (auto simp: post_def)

lemma pre_conj [simp]:
  "\<lfloor>Q \<inter> P\<rfloor> = \<lfloor>Q\<rfloor> \<inter> \<lfloor>P\<rfloor>"
  by (auto simp: pre_def)

lemma falling_rw:
  assumes "falling s \<L> P' Q" "P \<subseteq> P'"
  shows "falling s \<L> P Q"
  using assms unfolding falling_def by blast

subsection \<open>Action Proof Obligations\<close>

lemma act_po_rwI [intro]:
  assumes "P \<subseteq> P'" "Q' \<subseteq> Q" "G' \<subseteq> G"
  assumes "act_po G' s \<L> P' \<alpha> Q'"
  shows "act_po G s \<L> P \<alpha> Q"
  unfolding act_po_def
proof (intro conjI)
  show "progress s \<L> P \<alpha>" 
    using assms by (auto simp: act_po_def)
next
  show "\<lfloor>policy \<L> \<inter> P\<rfloor> \<inter> eval\<^sub>t s \<alpha> \<subseteq> G \<inter> \<lceil>policy \<L> \<inter> Q\<rceil>" 
    using assms unfolding  act_po_def
    using pre_subI[of "policy \<L> \<inter> P" "policy \<L> \<inter> P'"] post_subI[of "policy \<L> \<inter> Q'" "policy \<L> \<inter> Q"] 
    by blast
next
  show "falling s \<L> P \<alpha>"
    using assms falling_rw by (auto simp: act_po_def)
next
  show "defined s \<L> P \<alpha>"
    using assms apply (auto simp: act_po_def defined_def)
    by blast
qed

lemma act_po_frameI [intro]:
  assumes "stable \<L> M G"
  assumes "act_po G s \<L> P \<alpha> Q"
  shows "act_po G s \<L> (P \<inter> M) \<alpha> (Q \<inter> M)"
  unfolding act_po_def
proof (intro conjI)
  show "progress s \<L> (P \<inter> M) \<alpha>" 
    using assms by (auto simp: act_po_def)
next
  have "\<lfloor>policy \<L> \<inter> P\<rfloor> \<inter> eval\<^sub>t s \<alpha> \<subseteq> G \<inter> \<lceil>policy \<L> \<inter> Q\<rceil>"
    using assms by (auto simp: act_po_def)
  moreover have "\<lfloor>M\<rfloor> \<inter> \<lceil>policy \<L>\<rceil> \<inter> \<lfloor>policy \<L>\<rfloor> \<inter> G \<subseteq> \<lceil>M\<rceil>"
    using assms by (auto simp: stable_def pre_def post_def invPolicy_def preserve_def)
  ultimately show "\<lfloor>policy \<L> \<inter> (P \<inter> M)\<rfloor> \<inter> eval\<^sub>t s \<alpha> \<subseteq> G \<inter> \<lceil>policy \<L> \<inter> (Q \<inter> M)\<rceil>"
    by auto
next
  show "falling s \<L> (P \<inter> M) \<alpha>"
    using assms by (auto simp: falling_def act_po_def)
next
  show "defined s \<L> (P \<inter> M) \<alpha>"
    using assms by (auto simp: act_po_def defined_def)
qed

lemma act_po_falseI [intro]:
  "act_po G s \<L> {} \<alpha> {}" 
  by (auto simp: act_po_def pre_def falling_def progress_def defined_def)

lemma preserve_low_eq:
  assumes "(m\<^sub>1, m\<^sub>1') \<in> eval \<alpha>"
  assumes "(m\<^sub>2, m\<^sub>2') \<in> eval \<alpha>"
  assumes "m\<^sub>1 =\<^bsub>s,\<L>,P\<^esub> m\<^sub>2"
  assumes "act_po G s \<L> P \<alpha> Q"
  shows "m\<^sub>1' =\<^bsub>s,\<L>,Q\<^esub> m\<^sub>2'"
proof -
  have s: "\<lfloor>policy \<L> \<inter> P\<rfloor> \<inter> eval\<^sub>t s \<alpha> \<subseteq> G \<inter> \<lceil>policy \<L> \<inter> Q\<rceil>" 
    using assms(4) by (auto simp: act_po_def)
  have s': "\<forall>m \<Gamma> m'. (m,\<Gamma>) \<in> policy \<L> \<inter> P \<longrightarrow> (m,m') \<in> eval \<alpha> \<longrightarrow> s m' \<le> s m"
    using assms(4) by (auto simp: falling_def act_po_def)
  obtain \<Gamma>\<^sub>1 where g1: "(m\<^sub>1, \<Gamma>\<^sub>1) \<in> policy \<L>" "(m\<^sub>1, \<Gamma>\<^sub>1) \<in> P" "\<S> \<turnstile> m\<^sub>1 =\<^bsub>s,\<Gamma>\<^sub>1\<^esub> m\<^sub>2" 
    using assms(3) by (auto simp: low_equiv_def)
  then obtain \<Gamma>\<^sub>1' where g1': "(\<Gamma>\<^sub>1,\<Gamma>\<^sub>1') \<in> flow s \<alpha> m\<^sub>1"
    using assms(1,4) unfolding act_po_def defined_def
    by blast
  obtain \<Gamma>\<^sub>2 where g2: "(m\<^sub>2, \<Gamma>\<^sub>2) \<in> policy \<L>" "(m\<^sub>2, \<Gamma>\<^sub>2) \<in> P" "\<S> \<turnstile> m\<^sub>1 =\<^bsub>s,\<Gamma>\<^sub>2\<^esub> m\<^sub>2"
    using assms(3) by (auto simp: low_equiv_def)
  then obtain \<Gamma>\<^sub>2' where g2': "(\<Gamma>\<^sub>2,\<Gamma>\<^sub>2') \<in> flow s \<alpha> m\<^sub>2"
    using assms(2,4) unfolding act_po_def defined_def
    by blast
  have "(m\<^sub>1',\<Gamma>\<^sub>1') \<in> policy \<L> \<inter> Q" 
    using g1 g1' assms(1) s by (auto simp: eval\<^sub>t_def pre_def post_def) fast+
  moreover have "(m\<^sub>2',\<Gamma>\<^sub>2') \<in> policy \<L> \<inter> Q"
    using g2 g2' assms(2) s by (auto simp: eval\<^sub>t_def pre_def post_def) fast+
  moreover have "\<S> \<turnstile> m\<^sub>1' =\<^bsub>s,\<Gamma>\<^sub>1'\<^esub> m\<^sub>2'" 
    using assms(1,2) g1 g1' g2 flow_sound s' by blast
  moreover have "\<S> \<turnstile> m\<^sub>1' =\<^bsub>s,\<Gamma>\<^sub>2'\<^esub> m\<^sub>2'"
    using assms(1,2) low_equiv1_sym g1 g2 g2' flow_sound[OF assms(2,1)] s' by blast
  ultimately show ?thesis unfolding low_equiv_def by blast
qed

lemma bisim_act:
  assumes "(m\<^sub>1,m\<^sub>1') \<in> eval \<alpha>"
  assumes "m\<^sub>1 =\<^bsub>s,\<L>,P\<^esub> m\<^sub>2"
  assumes "act_po G s \<L> P \<alpha> Q"
  obtains m\<^sub>2' where "(m\<^sub>2, m\<^sub>2') \<in> eval \<alpha>" "m\<^sub>1' =\<^bsub>s,\<L>,Q\<^esub> m\<^sub>2'"
proof -
  obtain m\<^sub>2' where "(m\<^sub>2, m\<^sub>2') \<in> eval \<alpha>"
    using assms unfolding progress_def act_po_def by blast
  moreover have "m\<^sub>1' =\<^bsub>s,\<L>,Q\<^esub> m\<^sub>2'"
    using calculation assms(1,2,3) preserve_low_eq by auto
  ultimately show ?thesis using that by blast
qed

subsection \<open>Elimination Rules\<close>

lemma skipE [elim]:
  assumes "R,G,s \<turnstile>\<^sub>\<L> P {Skip} Q"
  obtains M where "stable \<L> M R" "P \<subseteq> M" "M \<subseteq> Q"
  using assms by (induct R G s \<L> P "Skip :: 'a Stmt" Q rule: rules.induct) blast+

lemma seqE [elim]:
  assumes "R,G,s \<turnstile>\<^sub>\<L> P {c\<^sub>1 ;; c\<^sub>2} Q"
  obtains M where "R,G,s \<turnstile>\<^sub>\<L> P {c\<^sub>1} M" "R,G,s \<turnstile>\<^sub>\<L> M {c\<^sub>2} Q"
  using assms by (induct R G s \<L> P "c\<^sub>1 ;; c\<^sub>2" Q arbitrary: c\<^sub>1 c\<^sub>2) blast+

lemma choiceE [elim]:
  assumes "R,G,s \<turnstile>\<^sub>\<L> P {c\<^sub>1 \<Sqinter> c\<^sub>2} Q"
  obtains "R,G,s \<turnstile>\<^sub>\<L> P {c\<^sub>1} Q" "R,G,s \<turnstile>\<^sub>\<L> P {c\<^sub>2} Q"
  using assms by (induct R G s \<L> P "c\<^sub>1 \<Sqinter> c\<^sub>2" Q arbitrary: c\<^sub>1 c\<^sub>2) auto

lemma loopE [elim]:
  assumes "R,G,s \<turnstile>\<^sub>\<L> P { c* } Q"
  obtains I where "P \<subseteq> I" "R,G,s \<turnstile>\<^sub>\<L> I { c } I" "I \<subseteq> Q" "stable \<L> I R"
  using assms 
proof (induct R G s \<L> P "c*" Q arbitrary: c)
  case (loop R G P c)
  then show ?case by blast
next
  case (rewrite R G s \<L> P Q P' R' G' Q')
  then show ?case 
    by (smt inf.cobounded2 inf.left_commute le_iff_inf order_trans rules.rewrite stableI)
qed

lemma actE [elim]:
  assumes "R,G,s \<turnstile>\<^sub>\<L> P { Basic \<alpha> } Q"
  obtains P' Q' where "P \<subseteq> P'" "Q' \<subseteq> Q" "stable \<L> P' R" "stable \<L> Q' R" "act_po G s \<L> P' \<alpha> Q'"
  using assms 
proof (induct R G s \<L> P "Basic \<alpha>" Q)
  case (act P R Q G)
  then show ?case by blast
next
  case (rewrite R G s \<L> P Q P' R' G' Q')
  show ?case
  proof (rule rewrite(2), goal_cases)
    case (1 P'' Q'')
    then show ?case using rewrite(3,4,5,6) rewrite(7)[of P'' Q''] 
      by (meson act_po_rwI order_refl stableI subset_trans)
  qed
qed

lemma parallelE [elim]:
  assumes "R,G,s \<turnstile>\<^sub>\<L> P { c\<^sub>1 || c\<^sub>2 } Q"
  obtains R\<^sub>1 G\<^sub>1 P\<^sub>1 Q\<^sub>1 R\<^sub>2 G\<^sub>2 P\<^sub>2 Q\<^sub>2 where  "R\<^sub>1,G\<^sub>1,s \<turnstile>\<^sub>\<L> P\<^sub>1 { c\<^sub>1 } Q\<^sub>1" "R\<^sub>2,G\<^sub>2,s \<turnstile>\<^sub>\<L> P\<^sub>2 { c\<^sub>2 } Q\<^sub>2"
    "P \<subseteq> P\<^sub>1 \<inter> P\<^sub>2" "R \<subseteq> R\<^sub>1 \<inter> R\<^sub>2" "G\<^sub>1 \<union> G\<^sub>2 \<subseteq> G" "Q\<^sub>1 \<inter> Q\<^sub>2 \<subseteq> Q" "G\<^sub>1 \<subseteq> R\<^sub>2" "G\<^sub>2 \<subseteq> R\<^sub>1"
  using assms
proof (induct R G s \<L> P "c\<^sub>1 || c\<^sub>2" Q)
  case (rewrite R G P Q P' R' G' Q')
  show ?case
  proof (rule rewrite(2), goal_cases)
    case (1 R\<^sub>1 G\<^sub>1 P\<^sub>1 Q\<^sub>1 R\<^sub>2 G\<^sub>2 P\<^sub>2 Q\<^sub>2)
    then show ?case using rewrite(3,4,5,6) rewrite(7)[OF 1(1,2)] by blast
  qed
next
  case (parallel R\<^sub>1 G\<^sub>2 G\<^sub>1 P\<^sub>1 Q\<^sub>1 R\<^sub>2 P\<^sub>2 Q\<^sub>2)
  then show ?case by (smt Int_lower2 inf_mono inf_sup_absorb order_refl sup_commute)
qed

subsection \<open>Introduction Rules\<close>

lemma falseI:
  shows "R,G,s \<turnstile>\<^sub>\<L> {} { c } {}" 
proof (induct c arbitrary: R G s \<L>)
  case Skip
  thus ?case by (auto simp: stable_def)
next
  case (Basic \<alpha>)
  thus ?case by (auto simp: stable_def)
next
  case (Loop c)
  thus ?case by (auto simp: stable_def)
next
  case (Parallel c1 c2)
  hence "(G \<union> R,R \<inter> G,s \<turnstile>\<^sub>\<L> {} {c1} {}) \<and> (R,R \<inter> G \<union> G \<inter> G,s \<turnstile>\<^sub>\<L> {} {c2} {})" by meson
  then show ?case by auto
qed blast+

lemma parallelI [intro]:
  assumes "R\<^sub>1,G\<^sub>1,s \<turnstile>\<^sub>\<L> P\<^sub>1 { c\<^sub>1 } Q\<^sub>1" "R\<^sub>2,G\<^sub>2,s \<turnstile>\<^sub>\<L> P\<^sub>2 { c\<^sub>2 } Q\<^sub>2"
    "P \<subseteq> P\<^sub>1 \<inter> P\<^sub>2" "R \<subseteq> R\<^sub>1 \<inter> R\<^sub>2" "G\<^sub>1 \<union> G\<^sub>2 \<subseteq> G" "Q\<^sub>1 \<inter> Q\<^sub>2 \<subseteq> Q" "G\<^sub>1 \<subseteq> R\<^sub>2" "G\<^sub>2 \<subseteq> R\<^sub>1"
  shows "R,G,s \<turnstile>\<^sub>\<L> P { c\<^sub>1 || c\<^sub>2 } Q"
  using assms by (meson parallel rules.rewrite)

lemma frameI [intro]:
  assumes "R,G,s \<turnstile>\<^sub>\<L> P {c} Q"
  assumes "stable \<L> M R\<^sub>2" "G \<subseteq> R\<^sub>2"
  shows "R \<inter> R\<^sub>2,G,s \<turnstile>\<^sub>\<L> P \<inter> M {c} Q \<inter> M"
  using assms
proof (induct arbitrary: M)
  case (parallel R\<^sub>1 G\<^sub>1 s \<L> P\<^sub>1 c\<^sub>1 Q\<^sub>1 R\<^sub>2' G\<^sub>2 P\<^sub>2 c\<^sub>2 Q\<^sub>2)
  hence a: "R\<^sub>1 \<inter> R\<^sub>2,G\<^sub>1,s \<turnstile>\<^sub>\<L> P\<^sub>1 \<inter> M {c\<^sub>1} Q\<^sub>1 \<inter> M" "R\<^sub>2' \<inter> R\<^sub>2,G\<^sub>2,s \<turnstile>\<^sub>\<L> P\<^sub>2 \<inter> M {c\<^sub>2} Q\<^sub>2 \<inter> M" by auto
  show ?case using parallel rules.parallel[OF a] by auto
qed blast+

lemma rwI [intro]:
  assumes "c \<leadsto> c'"
  assumes "R,G,s \<turnstile>\<^sub>\<L> P {c} Q"
  shows "R,G,s \<turnstile>\<^sub>\<L> P {c'} Q"
  using assms
proof (induct arbitrary: R G P Q)
  case (par1 c\<^sub>1 c\<^sub>1' c\<^sub>2)
  then obtain R\<^sub>1 G\<^sub>1 P\<^sub>1 Q\<^sub>1 R\<^sub>2 G\<^sub>2 P\<^sub>2 Q\<^sub>2 where a: "R\<^sub>1,G\<^sub>1,s \<turnstile>\<^sub>\<L> P\<^sub>1 { c\<^sub>1 } Q\<^sub>1" "R\<^sub>2,G\<^sub>2,s \<turnstile>\<^sub>\<L> P\<^sub>2 { c\<^sub>2 } Q\<^sub>2"
      "P \<subseteq> P\<^sub>1 \<inter> P\<^sub>2" "R \<subseteq> R\<^sub>1 \<inter> R\<^sub>2" "G\<^sub>1 \<union> G\<^sub>2 \<subseteq> G" "Q\<^sub>1 \<inter> Q\<^sub>2 \<subseteq> Q" "G\<^sub>1 \<subseteq> R\<^sub>2" "G\<^sub>2 \<subseteq> R\<^sub>1"
    by auto
  hence "R\<^sub>1,G\<^sub>1,s \<turnstile>\<^sub>\<L> P\<^sub>1 { c\<^sub>1' } Q\<^sub>1" using par1 by auto
  then show ?case using a by blast
next
  case (par2 c\<^sub>2 c\<^sub>2' c\<^sub>1)
  then obtain R\<^sub>1 G\<^sub>1 P\<^sub>1 Q\<^sub>1 R\<^sub>2 G\<^sub>2 P\<^sub>2 Q\<^sub>2 where a: "R\<^sub>1,G\<^sub>1,s \<turnstile>\<^sub>\<L> P\<^sub>1 { c\<^sub>1 } Q\<^sub>1" "R\<^sub>2,G\<^sub>2,s \<turnstile>\<^sub>\<L> P\<^sub>2 { c\<^sub>2 } Q\<^sub>2"
      "P \<subseteq> P\<^sub>1 \<inter> P\<^sub>2" "R \<subseteq> R\<^sub>1 \<inter> R\<^sub>2" "G\<^sub>1 \<union> G\<^sub>2 \<subseteq> G" "Q\<^sub>1 \<inter> Q\<^sub>2 \<subseteq> Q" "G\<^sub>1 \<subseteq> R\<^sub>2" "G\<^sub>2 \<subseteq> R\<^sub>1"
    by auto
  hence "R\<^sub>2,G\<^sub>2,s \<turnstile>\<^sub>\<L> P\<^sub>2 { c\<^sub>2' } Q\<^sub>2" using par2 by auto
  then show ?case using a by blast
next
  case (parE1 c)
  then obtain R\<^sub>1 G\<^sub>1 P\<^sub>1 Q\<^sub>1 R\<^sub>2 G\<^sub>2 P\<^sub>2 Q\<^sub>2 where a: "R\<^sub>1,G\<^sub>1,s \<turnstile>\<^sub>\<L> P\<^sub>1 { Skip } Q\<^sub>1" "R\<^sub>2,G\<^sub>2,s \<turnstile>\<^sub>\<L> P\<^sub>2 { c } Q\<^sub>2"
      "P \<subseteq> P\<^sub>1 \<inter> P\<^sub>2" "R \<subseteq> R\<^sub>1 \<inter> R\<^sub>2" "G\<^sub>1 \<union> G\<^sub>2 \<subseteq> G" "Q\<^sub>1 \<inter> Q\<^sub>2 \<subseteq> Q" "G\<^sub>1 \<subseteq> R\<^sub>2" "G\<^sub>2 \<subseteq> R\<^sub>1"
    by auto
  then obtain M where "stable \<L> M R\<^sub>1" "P\<^sub>1 \<subseteq> M" "M \<subseteq> Q\<^sub>1" by auto
  then show ?case using a by blast
next
  case (parE2 c)
  then obtain R\<^sub>1 G\<^sub>1 P\<^sub>1 Q\<^sub>1 R\<^sub>2 G\<^sub>2 P\<^sub>2 Q\<^sub>2 where a: "R\<^sub>1,G\<^sub>1,s \<turnstile>\<^sub>\<L> P\<^sub>1 { c } Q\<^sub>1" "R\<^sub>2,G\<^sub>2,s \<turnstile>\<^sub>\<L> P\<^sub>2 { Skip } Q\<^sub>2"
      "P \<subseteq> P\<^sub>1 \<inter> P\<^sub>2" "R \<subseteq> R\<^sub>1 \<inter> R\<^sub>2" "G\<^sub>1 \<union> G\<^sub>2 \<subseteq> G" "Q\<^sub>1 \<inter> Q\<^sub>2 \<subseteq> Q" "G\<^sub>1 \<subseteq> R\<^sub>2" "G\<^sub>2 \<subseteq> R\<^sub>1"
    by auto
  then obtain M where "stable \<L> M R\<^sub>2" "P\<^sub>2 \<subseteq> M" "M \<subseteq> Q\<^sub>2" by auto
  then show ?case using a by blast
next
  case (loop c n)
  then obtain I where s: "P \<subseteq> I" "R,G,s \<turnstile>\<^sub>\<L> I {c} I" "I \<subseteq> Q" "stable \<L> I R" by auto
  hence "R,G,s \<turnstile>\<^sub>\<L> I {unroll n c} I" by (induct n) auto
  then show ?case using s by blast
qed fast+

lemma multi_rwI [intro]:
  assumes "c \<mapsto>[]\<^sup>* c'"
  assumes "R,G,s \<turnstile>\<^sub>\<L> P {c} Q"
  shows "R,G,s \<turnstile>\<^sub>\<L> P {c'} Q"
  using assms by (induct c "[] :: 'a Trace" c') auto

lemma precontext_stable:
  assumes "R,G,s \<turnstile>\<^sub>\<L> P {c} Q"
  obtains P' where "P \<subseteq> P'" "stable \<L> P' R" "R,G,s \<turnstile>\<^sub>\<L> P' {c} Q"
  using assms 
proof (induct)
  case (choice R G s \<L> P c\<^sub>1 Q c\<^sub>2)
  show ?case
  proof (rule choice(2), rule choice(4),  goal_cases)
    case (1 P' P'')
    have a: "stable \<L> (P' \<inter> P'') R" using 1 by auto
    have b: "P \<subseteq> P' \<inter> P''" using 1 by auto
    then show ?case using 1 choice(5)[OF b a] by blast
  qed
next
  case (parallel R\<^sub>1 G\<^sub>1 P\<^sub>1 c\<^sub>1 Q\<^sub>1 R\<^sub>2 G\<^sub>2 P\<^sub>2 c\<^sub>2 Q\<^sub>2)
  show ?case
  proof (rule parallel(2), rule parallel(4), goal_cases)
    case (1 P' P'')
    then show ?case using parallel(7)[of "P' \<inter> P''"] parallel(5,6) by blast
  qed
qed blast+

lemma progI [intro]:
  assumes "c \<mapsto>\<^sub>\<alpha> c'"
  assumes "R,G,s \<turnstile>\<^sub>\<L> P {c} Q"
  shows "R,G,s \<turnstile>\<^sub>\<L> P {Basic \<alpha> ;; c'} Q"
  using assms
proof (induct arbitrary: R G P Q)
  case (act \<alpha>)
  then obtain P' Q' where a: "P \<subseteq> P'" "Q' \<subseteq> Q" "stable \<L> P' R" "stable \<L> Q' R" "act_po G s \<L> P' \<alpha> Q'"
    by auto
  hence "R,G,s \<turnstile>\<^sub>\<L> Q' {Skip} Q" by auto
  moreover have "R,G,s \<turnstile>\<^sub>\<L> P {Basic \<alpha>} Q'" using a by auto
  ultimately show ?case by auto
next
  case (seq c\<^sub>1 \<alpha> c\<^sub>1' c\<^sub>2)
  then obtain M where a: "R,G,s \<turnstile>\<^sub>\<L> P {c\<^sub>1} M" "R,G,s \<turnstile>\<^sub>\<L> M {c\<^sub>2} Q"
    by auto
  hence "R,G,s \<turnstile>\<^sub>\<L> P {Basic \<alpha> ;; c\<^sub>1'} M" using seq by auto
  then show ?case using a(2) by auto
next
  case (par1 c\<^sub>1 \<alpha> c\<^sub>1' c\<^sub>2)
  then obtain R\<^sub>1 G\<^sub>1 P\<^sub>1 Q\<^sub>1 R\<^sub>2 G\<^sub>2 P\<^sub>2 Q\<^sub>2 where a: "R\<^sub>1,G\<^sub>1,s \<turnstile>\<^sub>\<L> P\<^sub>1 { c\<^sub>1 } Q\<^sub>1" "R\<^sub>2,G\<^sub>2,s \<turnstile>\<^sub>\<L> P\<^sub>2 { c\<^sub>2 } Q\<^sub>2"
      "P \<subseteq> P\<^sub>1 \<inter> P\<^sub>2" "R \<subseteq> R\<^sub>1 \<inter> R\<^sub>2" "G\<^sub>1 \<union> G\<^sub>2 \<subseteq> G" "Q\<^sub>1 \<inter> Q\<^sub>2 \<subseteq> Q" "G\<^sub>1 \<subseteq> R\<^sub>2" "G\<^sub>2 \<subseteq> R\<^sub>1"
    by auto
  then obtain M where b: "R\<^sub>1,G\<^sub>1,s \<turnstile>\<^sub>\<L> P\<^sub>1 {Basic \<alpha>} M" "R\<^sub>1,G\<^sub>1,s \<turnstile>\<^sub>\<L> M {c\<^sub>1'} Q\<^sub>1" using par1 by force
  obtain P' where c: "P\<^sub>2 \<subseteq> P'" "stable \<L> P' R\<^sub>2" "R\<^sub>2,G\<^sub>2,s \<turnstile>\<^sub>\<L> P' {c\<^sub>2} Q\<^sub>2" 
    using a(2) precontext_stable by metis 
  have "R\<^sub>1 \<inter> R\<^sub>2,G\<^sub>1 \<union> G\<^sub>2,s \<turnstile>\<^sub>\<L> P\<^sub>1 \<inter> P\<^sub>2 {Basic \<alpha>} M \<inter> P'"
    using frameI[OF b(1) c(2) a(7)] c(1) by blast
  moreover have "R\<^sub>1 \<inter> R\<^sub>2,G\<^sub>1 \<union> G\<^sub>2,s \<turnstile>\<^sub>\<L> M \<inter> P' {c\<^sub>1' || c\<^sub>2} Q\<^sub>1 \<inter> Q\<^sub>2" 
    using b(2) c(3) a(7,8) by blast
  ultimately show ?case using a(3,4,5,6) by blast
next
  case (par2 c\<^sub>2 \<alpha> c\<^sub>2' c\<^sub>1)
  then obtain R\<^sub>1 G\<^sub>1 P\<^sub>1 Q\<^sub>1 R\<^sub>2 G\<^sub>2 P\<^sub>2 Q\<^sub>2 where a: "R\<^sub>1,G\<^sub>1,s \<turnstile>\<^sub>\<L> P\<^sub>1 { c\<^sub>1 } Q\<^sub>1" "R\<^sub>2,G\<^sub>2,s \<turnstile>\<^sub>\<L> P\<^sub>2 { c\<^sub>2 } Q\<^sub>2"
      "P \<subseteq> P\<^sub>1 \<inter> P\<^sub>2" "R \<subseteq> R\<^sub>1 \<inter> R\<^sub>2" "G\<^sub>1 \<union> G\<^sub>2 \<subseteq> G" "Q\<^sub>1 \<inter> Q\<^sub>2 \<subseteq> Q" "G\<^sub>1 \<subseteq> R\<^sub>2" "G\<^sub>2 \<subseteq> R\<^sub>1"
    by auto
  then obtain M where b: "R\<^sub>2,G\<^sub>2,s \<turnstile>\<^sub>\<L> P\<^sub>2 {Basic \<alpha>} M" "R\<^sub>2,G\<^sub>2,s \<turnstile>\<^sub>\<L> M {c\<^sub>2'} Q\<^sub>2" using par2 by force
  obtain P' where c: "P\<^sub>1 \<subseteq> P'" "stable \<L> P' R\<^sub>1" "R\<^sub>1,G\<^sub>1,s \<turnstile>\<^sub>\<L> P' {c\<^sub>1} Q\<^sub>1" 
    using a(1) precontext_stable by metis 
  have "R\<^sub>1 \<inter> R\<^sub>2,G\<^sub>1 \<union> G\<^sub>2,s \<turnstile>\<^sub>\<L> P\<^sub>1 \<inter> P\<^sub>2 {Basic \<alpha>} M \<inter> P'"
    using frameI[OF b(1) c(2) a(8)] c(1) by blast
  moreover have "R\<^sub>1 \<inter> R\<^sub>2,G\<^sub>1 \<union> G\<^sub>2,s \<turnstile>\<^sub>\<L> M \<inter> P' {c\<^sub>1 || c\<^sub>2'} Q\<^sub>1 \<inter> Q\<^sub>2" 
    using b(2) c(3) a(7,8) by blast
  ultimately show ?case using a(3,4,5,6) by blast
qed

lemma multi_progI [intro]:
  assumes "c \<mapsto>[\<alpha>]\<^sup>* c'"
  assumes "R,G,s \<turnstile>\<^sub>\<L> P {c} Q"
  shows "R,G,s \<turnstile>\<^sub>\<L> P {Basic \<alpha> ;; c'} Q"
  using assms
proof (induct c "[\<alpha>]" c' arbitrary: R G P Q)
  case (rewrite c\<^sub>1 c\<^sub>2 c\<^sub>3)
  then show ?case by blast
next
  case (prepend c\<^sub>1 c\<^sub>2 c\<^sub>3)
  then show ?case by (meson multi_rwI seqE progI seq)
qed

lemma info_type_progE:
  assumes "c \<mapsto>[\<alpha>]\<^sup>* c'"
  assumes "R,G,s \<turnstile>\<^sub>\<L> P {c} Q"
  obtains M where "act_po G s \<L> P \<alpha> M" "R,G,s \<turnstile>\<^sub>\<L> M {c'} Q"
proof -
  have "R,G,s \<turnstile>\<^sub>\<L> P {Basic \<alpha> ;; c'} Q" using assms multi_progI by auto
  then obtain M where c: "R,G,s \<turnstile>\<^sub>\<L> P {Basic \<alpha>} M" "R,G,s \<turnstile>\<^sub>\<L> M {c'} Q" by auto
  then obtain P' M' where b: "P \<subseteq> P'" "M' \<subseteq> M" "stable \<L> P' R" "stable \<L> M' R" "act_po G s \<L> P' \<alpha> M'"
    by auto
  hence "act_po G s \<L> P \<alpha> M" by blast
  thus ?thesis using that c by auto
qed

lemma bisim_exists:
  assumes "R,G,s \<turnstile>\<^sub>\<L> P {c} Q"
  assumes "m\<^sub>1 =\<^bsub>s,\<L>,P\<^esub> m\<^sub>2"
  assumes "\<langle>c,m\<^sub>1\<rangle> \<rightarrow>t\<^sup>* \<langle>c\<^sub>1,m\<^sub>1'\<rangle>"
  obtains c\<^sub>2 m\<^sub>2' where "\<langle>c,m\<^sub>2\<rangle> \<rightarrow>t\<^sup>* \<langle>c\<^sub>2,m\<^sub>2'\<rangle>" "m\<^sub>1' =\<^bsub>s,\<L>,UNIV\<^esub> m\<^sub>2'"
  using assms unfolding ev_def
proof safe
  assume "trace_mem m\<^sub>1 t m\<^sub>1'" "c \<mapsto>t\<^sup>* c\<^sub>1" "R,G,s \<turnstile>\<^sub>\<L> P {c} Q" "m\<^sub>1 =\<^bsub>s,\<L>,P\<^esub> m\<^sub>2"
  hence "\<exists>m\<^sub>2' c\<^sub>2. (trace_mem m\<^sub>2 t m\<^sub>2' \<and> c \<mapsto>t\<^sup>* c\<^sub>2) \<and> m\<^sub>1' =\<^bsub>s,\<L>,UNIV\<^esub> m\<^sub>2'"
  proof (induct m\<^sub>1 t m\<^sub>1' arbitrary: P m\<^sub>2 c rule: trace_mem.induct)
    case (1 m)
    thus ?case using low_equiv_subI unfolding ev_def by blast
  next
    case (2 m\<^sub>1 m\<^sub>1' \<alpha> t m\<^sub>1'')
    then obtain c' where f1: "c \<mapsto>[\<alpha>]\<^sup>* c'" "c' \<mapsto>t\<^sup>* c\<^sub>1" by auto
    obtain M where f2: "act_po G s \<L> P \<alpha> M" "R,G,s \<turnstile>\<^sub>\<L> M {c'} Q"
      using info_type_progE[OF f1(1) 2(5)] by metis
    then obtain m\<^sub>2' where f3: "(m\<^sub>2,m\<^sub>2') \<in> eval \<alpha>" "m\<^sub>1' =\<^bsub>s,\<L>,M\<^esub> m\<^sub>2'"
      using 2(1,6) bisim_act by metis
    hence "\<exists>m\<^sub>2'' c\<^sub>2. (trace_mem m\<^sub>2' t m\<^sub>2'' \<and> c' \<mapsto>t\<^sup>* c\<^sub>2) \<and> m\<^sub>1'' =\<^bsub>s,\<L>,UNIV\<^esub> m\<^sub>2''" 
      using 2(3)[OF f1(2) f2(2)] by auto
    then show ?case using f3(1) 2(4) by blast
  qed
  thus ?thesis using that unfolding ev_def by auto
qed

theorem secure_bisim:
  assumes "R,G,s \<turnstile>\<^sub>\<L> P { c } Q"
  shows "secure s \<L> P c"
  using bisim_exists[OF assms] ev_det 
  unfolding secure_def by metis

end

end