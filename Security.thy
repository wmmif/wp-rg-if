theory Security
  imports Execution Base
begin

subsection \<open>Types\<close>

text \<open>A TPolicy represents a security policy mapping for a variable\<close>
type_synonym ('m,'i,'s) TPolicy = "'m \<Rightarrow> ('i,'s) Ctx"

text \<open>A TState represents a state, containing both the memory and type context\<close>
type_synonym ('m,'i,'s) TState = "('m \<times> ('i,'s) Ctx) set"

text \<open>A TStep represents a step, transitioning both the memory and type context\<close>
type_synonym ('m,'i,'s) TStep = "('m \<times> ('i,'s) Ctx) rel"

type_synonym ('m,'s) TType = "'m \<Rightarrow> 's"

subsection \<open>Definitions\<close>

text \<open>Preserve a state across a step\<close>
definition preserve :: "('Var,'Val,'Sec) TState \<Rightarrow> (_,_,_) TStep"
  where "preserve P \<equiv> {((m,\<Gamma>),(m',\<Gamma>')). (m,\<Gamma>) \<in> P \<longrightarrow> (m',\<Gamma>') \<in> P}"

text \<open>Convert a policy into a state that asserts it\<close>
definition policy :: "('Var,'Val,'Sec::bounded_lattice) TPolicy \<Rightarrow> ('Var,'Val,'Sec) TState"
  where "policy \<L> \<equiv> {(m,\<Gamma>). \<forall>x. \<L> m x \<ge> \<Gamma> x}"

text \<open>Convert a policy into a step that preserves it\<close>
definition invPolicy :: "('Var,'Val,'Sec::bounded_lattice) TPolicy \<Rightarrow> ('Var,'Val,'Sec) TStep"
  where "invPolicy \<L> \<equiv> preserve (policy \<L>)"

text \<open>Describe low equivalence between two memories for one \<Gamma>\<close>
definition low_equiv1 :: "('i \<Rightarrow> 'm rel) \<Rightarrow> 'm \<Rightarrow> ('m,'s::bounded_lattice) TType \<Rightarrow> ('i,'s) Ctx \<Rightarrow> 'm \<Rightarrow> bool"
  ("_ \<turnstile>_ =\<^bsub>_,_\<^esub> _" [70,70,0,0,70] 100)
  where "k \<turnstile> m\<^sub>1 =\<^bsub>s,\<Gamma>\<^esub> m\<^sub>2 \<equiv> \<forall>x. (\<Gamma> x \<le> s m\<^sub>1 \<or> \<Gamma> x \<le> s m\<^sub>2) \<longrightarrow> (m\<^sub>1,m\<^sub>2) \<in> k x"

locale security = execution eval
  for eval :: "'a \<Rightarrow> 'm rel" +
  fixes \<S> :: "'i \<Rightarrow> 'm rel"
  assumes \<S>_sym: "\<forall>i. sym (\<S> i)"

context security
begin

text \<open>Describe low equivalence between two memories for an analysis state\<close>
definition low_equiv :: "'m \<Rightarrow> ('m,'s::bounded_lattice) TType \<Rightarrow> ('m,'i,'s) TPolicy \<Rightarrow> ('m,'i,'s) TState \<Rightarrow> 'm \<Rightarrow> bool"  ("_ =\<^bsub>_,_,_\<^esub> _" [70,0,0,0,70] 100)
  where "m\<^sub>1 =\<^bsub>s,\<L>,P\<^esub> m\<^sub>2 \<equiv> \<exists>\<Gamma>\<^sub>1 \<Gamma>\<^sub>2. (m\<^sub>1,\<Gamma>\<^sub>1) \<in> policy \<L> \<inter> P \<and> (m\<^sub>2,\<Gamma>\<^sub>2) \<in> policy \<L> \<inter> P \<and> \<S> \<turnstile> m\<^sub>1 =\<^bsub>s,\<Gamma>\<^sub>1\<^esub> m\<^sub>2 \<and> \<S> \<turnstile> m\<^sub>1 =\<^bsub>s,\<Gamma>\<^sub>2\<^esub> m\<^sub>2"

text \<open>The security bisimulation property we are enforcing\<close>
definition secure :: "('m,'s::bounded_lattice) TType \<Rightarrow> ('m,'i,'s) TPolicy \<Rightarrow> ('m,'i,'s) TState \<Rightarrow> 'a Stmt \<Rightarrow> bool"  where "secure s \<L> P c \<equiv> \<forall>m\<^sub>1 m\<^sub>2 c\<^sub>1 t m\<^sub>1'. 
          \<langle>c,m\<^sub>1\<rangle> \<rightarrow>t\<^sup>* \<langle>c\<^sub>1,m\<^sub>1'\<rangle> \<longrightarrow> m\<^sub>1 =\<^bsub>s,\<L>,P\<^esub> m\<^sub>2 \<longrightarrow> 
          ((\<exists>m\<^sub>2' c\<^sub>2. \<langle>c,m\<^sub>2\<rangle> \<rightarrow>t\<^sup>* \<langle>c\<^sub>2,m\<^sub>2'\<rangle>) \<and>
          (\<forall>m\<^sub>2' c\<^sub>2. \<langle>c,m\<^sub>2\<rangle> \<rightarrow>t\<^sup>* \<langle>c\<^sub>2,m\<^sub>2'\<rangle> \<longrightarrow> m\<^sub>1' =\<^bsub>s,\<L>,UNIV\<^esub> m\<^sub>2'))"

lemma low_equiv1_sym [sym]:
  "\<S> \<turnstile> m\<^sub>1 =\<^bsub>s,\<Gamma>\<^esub> m\<^sub>2 \<Longrightarrow> \<S> \<turnstile> m\<^sub>2 =\<^bsub>s,\<Gamma>\<^esub> m\<^sub>1"
  using \<S>_sym unfolding low_equiv1_def sym_def by auto

lemma low_equiv_subI [intro]:
  "m\<^sub>1 =\<^bsub>s,\<L>,P\<^esub> m\<^sub>2 \<Longrightarrow> P \<subseteq> Q \<Longrightarrow> m\<^sub>1 =\<^bsub>s,\<L>,Q\<^esub> m\<^sub>2"
proof -
  assume a: "P \<subseteq> Q" "m\<^sub>1 =\<^bsub>s,\<L>,P\<^esub> m\<^sub>2"
  hence "policy \<L> \<inter> P \<subseteq> policy \<L> \<inter> Q" by auto
  thus ?thesis using a unfolding low_equiv_def by blast
qed

lemma low_equiv_nil [elim!]:
  assumes "m\<^sub>1 =\<^bsub>s,\<L>,{}\<^esub> m\<^sub>2"
  obtains "False"
  using assms by (auto simp: low_equiv_def)

text \<open>The security bisimulation property is symmetric\<close>
theorem low_equiv_sym [sym]:
  "m\<^sub>1 =\<^bsub>s,\<L>,P\<^esub> m\<^sub>2 \<Longrightarrow> m\<^sub>2 =\<^bsub>s,\<L>,P\<^esub> m\<^sub>1"
  unfolding low_equiv_def using low_equiv1_sym by blast

end

end