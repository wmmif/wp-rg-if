theory Primed_Typed_Predicate_Language
  imports Typed_Predicate_Language
begin                   

text \<open>
Extend the predicate language with a concept of original and primed variables
\<close>

datatype 'var Primed = 
  Orig "'var" ("_\<^sup>o" [1000] 1000)
| Prime "'var" ("_\<^sup>p" [1000] 1000)

type_synonym ('var,'val,'sec) rexp = "('var Primed,'val,'sec) exp"
type_synonym ('var,'val,'sec) rpred = "('var Primed,'val,'sec) pred"
type_synonym ('var,'val,'sec) rsecexp = "('var Primed,'val,'sec) secexp"

abbreviation transition (infixr "\<mapsto>" 50)
  where "transition m\<^sub>1 m\<^sub>2 \<equiv> case_Primed m\<^sub>1 m\<^sub>2"

definition reflexive :: "('var,'val,'sec) rpred \<Rightarrow> bool"
  where "reflexive R \<equiv> \<forall>m \<Gamma>. test (m \<mapsto> m) (\<Gamma> \<mapsto> \<Gamma>) R"

definition transitive :: "('var,'val,'sec) rpred \<Rightarrow> bool"
  where "transitive R \<equiv> \<forall>m \<Gamma> m' \<Gamma>' m'' \<Gamma>''. 
    test (m \<mapsto> m') (\<Gamma> \<mapsto> \<Gamma>') R \<longrightarrow> test (m' \<mapsto> m'') (\<Gamma>' \<mapsto> \<Gamma>'') R \<longrightarrow> test (m \<mapsto> m'') (\<Gamma> \<mapsto> \<Gamma>'') R"

definition preserved :: "('var,'val,'sec) pred \<Rightarrow> ('var,'val,'sec) rpred"
  where "preserved P \<equiv> map\<^sub>p Orig P \<longrightarrow>\<^sub>p  map\<^sub>p Prime P"

(* Functions to lift predicates over one memory to two *)
definition orig\<^sub>e :: "('var,'val,'sec) exp \<Rightarrow> ('var,'val,'sec) rexp"
  where "orig\<^sub>e P \<equiv> map\<^sub>e Orig P"

definition orig :: "('var,'val,'sec) pred \<Rightarrow> ('var,'val,'sec) rpred"
  where "orig P \<equiv> map\<^sub>p Orig P"

definition orig\<^sub>s :: "('var,'val,'sec) secexp \<Rightarrow> ('var,'val,'sec) rsecexp"
  where "orig\<^sub>s P \<equiv> map\<^sub>s Orig P"

definition prime :: "('var,'val,'sec) pred \<Rightarrow> ('var,'val,'sec) rpred"
  where "prime P \<equiv> map\<^sub>p Prime P"

(* Functions to drop predicates over two memories to one *)
definition unprime :: "('var,'val,'sec) rpred \<Rightarrow> ('var,'val,'sec) pred"
  where "unprime P \<equiv> map\<^sub>p (case_Primed id id) P"

lemma test_unprime [simp]:
  "test m \<Gamma> (unprime G) = (test (m \<mapsto> m) (\<Gamma> \<mapsto> \<Gamma>) G)"
proof -
  have "test (m \<mapsto> m) (\<Gamma> \<mapsto> \<Gamma>) G = test  m \<Gamma> (unprime G)"
    by (auto intro!: map_vars_det simp: unprime_def split: Primed.splits)
  thus ?thesis by auto
qed

lemma [simp]:
  "(m \<mapsto> m')(x\<^sup>p := e) = (m \<mapsto> (m'(x := e)))" (is "?l = ?r")
proof -
  have "\<forall>x. ?l x = ?r x" by (auto split: Primed.splits)
  thus ?thesis by blast
qed

lemma [simp]:
  "eval (m \<mapsto> m') (\<Gamma> \<mapsto> \<Gamma>') (orig\<^sub>e e) = eval m \<Gamma> e"
proof -
  have "eval m \<Gamma> e = eval (m \<mapsto> m') (\<Gamma> \<mapsto> \<Gamma>') (orig\<^sub>e e)"
    by (auto intro!: map_vars_det simp: orig\<^sub>e_def)
  thus ?thesis by auto
qed

lemma [simp]:
  "test (m \<mapsto> m') (\<Gamma> \<mapsto> \<Gamma>') (orig e) = test m \<Gamma> e"
proof -
  have "test m \<Gamma> e = test (m \<mapsto> m') (\<Gamma> \<mapsto> \<Gamma>') (orig e)"
    by (auto intro!: map_vars_det simp: orig_def)
  thus ?thesis by auto
qed

lemma [simp]:
  "eval\<^sub>s (m \<mapsto> m') (\<Gamma> \<mapsto> \<Gamma>') (orig\<^sub>s P) = eval\<^sub>s m \<Gamma> P"
proof -
  have "eval\<^sub>s m \<Gamma> P = eval\<^sub>s (m \<mapsto> m') (\<Gamma> \<mapsto> \<Gamma>') (orig\<^sub>s P)"
    by (auto intro!: map_vars_det simp: orig\<^sub>s_def)
  thus ?thesis by auto
qed

abbreviation wp\<^sub>a
  where "wp\<^sub>a x e t Q \<equiv> subst\<^sub>\<Gamma>\<^sub>p (subst\<^sub>p Q x ( e)) x ( t)"

abbreviation wp\<^sub>b
  where "wp\<^sub>b b Q \<equiv>  b \<longrightarrow>\<^sub>p Q"

definition wp\<^sub>l
  where "wp\<^sub>l x i e t \<equiv> foldi 0 (\<lambda>j v. wp\<^sub>a v (Ternary (PCmp (=) i (Const j)) e (Var v)) (STer (PCmp (=) i (Const j)) t (Type v))) x"

abbreviation orig_state
  where "orig_state m \<equiv> (\<lambda>x. m (Orig x))"

abbreviation prime_state
  where "prime_state m \<equiv> (\<lambda>x. m (Prime x))"

lemma mem_eqI [intro]:
  "(\<forall>x. f (Orig x) = q (Orig x)) \<Longrightarrow> (\<forall>x. f (Prime x) = q (Prime x)) \<Longrightarrow> f = q"
  by (subgoal_tac "\<forall>x. f x = q x") (auto, case_tac x, auto)

lemma [simp]:
  "- range Prime = range Orig"
  by (auto, case_tac x, auto)

lemma [simp]:
  "(m \<mapsto> m') x\<^sup>o = m x"
  by auto

lemma test_mem_eqI:
  " test m \<Gamma> P \<Longrightarrow>  (\<forall>x. m x = m' x) \<Longrightarrow> (\<forall>x. \<Gamma> x = \<Gamma>' x) \<Longrightarrow>test m' \<Gamma>' P"  
  by (metis vars_det(1))

lemma [simp]:
  "override_on (m \<mapsto> m') m'' (range Prime) = (m \<mapsto> prime_state m'')"
  unfolding override_on_def by auto
   
lemma test_pall_prime [simp]:
  "set l = range Prime \<Longrightarrow> test (m \<mapsto> m') (\<Gamma> \<mapsto> \<Gamma>') (pall l P) = (\<forall>m' \<Gamma>'. test (m \<mapsto> m') (\<Gamma> \<mapsto> \<Gamma>') P)"
proof (clarsimp, safe, goal_cases)
  case (1 m'' \<Gamma>'')
  have "prime_state (transition m m'') = m''" "prime_state (transition \<Gamma> \<Gamma>'') = \<Gamma>''" by auto
  then show ?case using 1(2) by metis
next
  case (2 m' \<Gamma>')
  hence a: "m' = (orig_state m' \<mapsto> prime_state m')" "\<Gamma>' = (orig_state \<Gamma>' \<mapsto> prime_state \<Gamma>')" by auto
  have "test (m \<mapsto> prime_state m') (\<Gamma> \<mapsto> prime_state \<Gamma>') P" using 2 by auto
  thus ?case unfolding override_on_def
    apply - by (rule test_mem_eqI) (auto; case_tac x; auto)+
qed

lemma test_prime [simp]:
  "test (m \<mapsto> m') (\<Gamma> \<mapsto> \<Gamma>') (prime P) = test m' \<Gamma>' P"
proof -
  have "test m' \<Gamma>' P = test (m \<mapsto> m') (\<Gamma> \<mapsto> \<Gamma>') (prime P)"
    by (auto intro!: map_vars_det simp: prime_def split: Primed.splits)
  thus ?thesis by auto
qed

text \<open>Show that wpl is the weakest precondition for a list store\<close>
lemma asn_foldi_nop [simp]:
  assumes "eval m \<Gamma> i < k"
  shows "test m \<Gamma> (foldi k (\<lambda>j v Q. subst\<^sub>\<Gamma>\<^sub>p (subst\<^sub>p Q v ( (Ternary (PCmp (=) i (Const j)) e (Var v)))) v ( (STer (PCmp (=) i (Const j)) t (Type v)))) x Q) = test m \<Gamma> Q"
  using assms by (induct x arbitrary: k Q) simp+

lemma wp\<^sub>l [simp]:
  assumes "eval m \<Gamma> i < length x"
  assumes "set x \<inter> vars\<^sub>e i = {}"
  assumes "set x \<inter> vars\<^sub>\<Gamma>\<^sub>e i = {}"
  assumes "vars\<^sub>\<Gamma>\<^sub>e e = {}"
  shows "test m \<Gamma> (wp\<^sub>l x i e t Q) = test (m(x ! eval m \<Gamma> i := eval m \<Gamma> e)) (\<Gamma>(x ! eval m \<Gamma> i := eval\<^sub>s m \<Gamma> t)) Q"
proof -
  have "\<And>k Q. k \<le> eval m \<Gamma> i \<Longrightarrow> eval m \<Gamma> i - k < length x \<Longrightarrow> set x \<inter> vars\<^sub>e i = {} \<Longrightarrow> set x \<inter> vars\<^sub>\<Gamma>\<^sub>e i = {} \<Longrightarrow> vars\<^sub>\<Gamma>\<^sub>e e = {} \<Longrightarrow> test m \<Gamma> (foldi k (\<lambda>j v. wp\<^sub>a v (Ternary (PCmp (=) i (Const j)) e (Var v)) (STer (PCmp (=) i (Const j)) t (Type v))) x Q) = test (m(x ! (eval m \<Gamma> i - k) := eval m \<Gamma> e)) (\<Gamma>(x ! (eval m \<Gamma> i - k) := eval\<^sub>s m \<Gamma> t)) Q"
  proof (induct x)
    case (Cons a x)
    then show ?case
    proof (cases "eval m \<Gamma> i = k")
      case True
      moreover have "eval m \<Gamma> i = eval m (\<Gamma>(a := eval\<^sub>s m \<Gamma> t)) i"
        using Cons(5) by (simp add: vars_det(2))
      moreover have "eval m \<Gamma> e = eval m (\<Gamma>(a := eval\<^sub>s m \<Gamma> t)) e"
        using Cons(6) by (simp add: vars_det(2))
      moreover have "m(a := eval m \<Gamma> e) = (\<lambda>b. if b = a then eval m \<Gamma> e else m b)" by auto
      moreover have "\<Gamma>(a := eval\<^sub>s m \<Gamma> t) = (\<lambda>b. if b = a then eval\<^sub>s m \<Gamma> t else \<Gamma> b)" by auto
      ultimately show ?thesis by auto
    next
      case False
      have "test m \<Gamma> (foldi (Suc k) (\<lambda>j v. wp\<^sub>a v (Ternary (PCmp (=) i (Const j)) e (Var v)) (STer (PCmp (=) i (Const j)) t (Type v))) x (subst\<^sub>\<Gamma>\<^sub>p (subst\<^sub>p Q a ( (Ternary (PCmp (=) i (Const k)) e (Var a)))) a ( (STer (PCmp (=) i (Const k)) t (Type a))))) = 
            test (m(x ! (eval m \<Gamma> i -(Suc k)) := eval m \<Gamma> e)) (\<Gamma>(x ! (eval m \<Gamma> i - (Suc k)) := eval\<^sub>s m \<Gamma> t)) (subst\<^sub>\<Gamma>\<^sub>p (subst\<^sub>p Q a ( (Ternary (PCmp (=) i (Const k)) e (Var a)))) a ( (STer (PCmp (=) i (Const k)) t (Type a))))"
        using False Cons(2,3,4,5,6) by (intro Cons(1)) auto
      also have "... = test (m((a # x) ! (eval m \<Gamma> i - k) := eval m \<Gamma> e)) (\<Gamma>((a # x) ! (eval m \<Gamma> i - k) := eval\<^sub>s m \<Gamma> t)) Q"
      proof -
        have noMod: "eval (m(x ! (eval m \<Gamma> i - Suc k) := eval m \<Gamma> e)) (\<Gamma>(x ! (eval m \<Gamma> i - Suc k) := eval\<^sub>s m \<Gamma> t)) i = eval m \<Gamma> i"
        proof (intro vars_det ballI impI)
          fix v assume a: "v \<in> vars\<^sub>e i"
          have "x ! (eval m \<Gamma> i - Suc k) \<in> set x" using Cons False by auto
          hence "v \<noteq> x ! (eval m \<Gamma> i - Suc k)" using Cons(4) a by auto
          thus "(m(x ! (eval m \<Gamma> i - Suc k) := eval m \<Gamma> e)) v = m v" by auto
        next
          fix v assume a: "v \<in> vars\<^sub>\<Gamma>\<^sub>e i"
          have "x ! (eval m \<Gamma> i - Suc k) \<in> set x" using Cons False by auto
          hence "v \<noteq> x ! (eval m \<Gamma> i - Suc k)" using Cons(5) a by auto
          thus "(\<Gamma>(x ! (eval m \<Gamma> i - Suc k) := eval\<^sub>s m \<Gamma> t)) v = \<Gamma> v" by auto
        qed  
        have "(a # x) ! (eval m \<Gamma> i - k) = x ! (eval m \<Gamma> i - Suc k)" using Cons(2) False by auto
        thus ?thesis using noMod False by (auto simp: fun_upd_twist)
      qed
      finally show ?thesis by fastforce
    qed
  qed simp
  thus ?thesis using assms by (auto simp: wp\<^sub>l_def)
qed

lemma [intro]:
  "P \<turnstile>\<^sub>p Q \<Longrightarrow> subst\<^sub>p P x e  \<turnstile>\<^sub>p subst\<^sub>p Q x e"
  by (simp add: entail_def)

lemma [intro]:
  "P \<turnstile>\<^sub>p Q \<Longrightarrow> subst\<^sub>\<Gamma>\<^sub>p P x e  \<turnstile>\<^sub>p subst\<^sub>\<Gamma>\<^sub>p Q x e"
  by (simp add: entail_def)

lemma [intro]:
  assumes "P \<turnstile>\<^sub>p Q"
  shows "wp\<^sub>a x e t P \<turnstile>\<^sub>p wp\<^sub>a x e t Q"
  using assms by auto

lemma [intro]:
  assumes "P \<turnstile>\<^sub>p Q"
  shows "(wp\<^sub>l x i e t P) \<turnstile>\<^sub>p (wp\<^sub>l x i e t Q)"
proof -
  have "\<And>m \<Gamma> k. 
          test m \<Gamma> (foldi k (\<lambda>j v. wp\<^sub>a v (Ternary (PCmp (=) i (Const j)) e (Var v)) (STer (PCmp (=) i (Const j)) t (Type v))) x P) \<longrightarrow> 
          test m \<Gamma> (foldi k (\<lambda>j v. wp\<^sub>a v (Ternary (PCmp (=) i (Const j)) e (Var v)) (STer (PCmp (=) i (Const j)) t (Type v))) x Q)" 
    using assms
  proof (induct x arbitrary: P Q)
    case Nil
    then show ?case by (auto simp: entail_def)
  next
    case (Cons a x)
    show ?case by (simp, rule Cons, insert Cons(2), auto)
  qed
  thus ?thesis by (auto simp: wp\<^sub>l_def entail_def)
qed

lemma conj_entail [intro]:
  assumes "P \<turnstile>\<^sub>p Q" "M \<turnstile>\<^sub>p N"
  shows "P \<and>\<^sub>p M \<turnstile>\<^sub>p Q \<and>\<^sub>p N"
  using assms by (auto simp: entail_def) 

end